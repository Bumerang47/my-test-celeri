create table django_migrations
(
    id serial not null
        constraint django_migrations_pkey
            primary key,
    app varchar(255) not null,
    name varchar(255) not null,
    applied timestamp with time zone not null
)
;

create table django_content_type
(
    id serial not null
        constraint django_content_type_pkey
            primary key,
    app_label varchar(100) not null,
    model varchar(100) not null,
    constraint django_content_type_app_label_model_76bd3d3b_uniq
        unique (app_label, model)
)
;

create table auth_permission
(
    id serial not null
        constraint auth_permission_pkey
            primary key,
    name varchar(255) not null,
    content_type_id integer not null
        constraint auth_permission_content_type_id_2f476e4b_fk_django_co
            references django_content_type
                deferrable initially deferred,
    codename varchar(100) not null,
    constraint auth_permission_content_type_id_codename_01ab375a_uniq
        unique (content_type_id, codename)
)
;

create index auth_permission_content_type_id_2f476e4b
    on auth_permission (content_type_id)
;

create table auth_group
(
    id serial not null
        constraint auth_group_pkey
            primary key,
    name varchar(80) not null
        constraint auth_group_name_key
            unique
)
;

create index auth_group_name_a6ea08ec_like
    on auth_group (name)
;

create table auth_group_permissions
(
    id serial not null
        constraint auth_group_permissions_pkey
            primary key,
    group_id integer not null
        constraint auth_group_permissions_group_id_b120cbf9_fk_auth_group_id
            references auth_group
                deferrable initially deferred,
    permission_id integer not null
        constraint auth_group_permissio_permission_id_84c5c92e_fk_auth_perm
            references auth_permission
                deferrable initially deferred,
    constraint auth_group_permissions_group_id_permission_id_0cd325b0_uniq
        unique (group_id, permission_id)
)
;

create index auth_group_permissions_group_id_b120cbf9
    on auth_group_permissions (group_id)
;

create index auth_group_permissions_permission_id_84c5c92e
    on auth_group_permissions (permission_id)
;

create table auth_user
(
    id serial not null
        constraint auth_user_pkey
            primary key,
    password varchar(128) not null,
    last_login timestamp with time zone,
    is_superuser boolean not null,
    username varchar(150) not null
        constraint auth_user_username_key
            unique,
    first_name varchar(30) not null,
    last_name varchar(30) not null,
    email varchar(254) not null,
    is_staff boolean not null,
    is_active boolean not null,
    date_joined timestamp with time zone not null
)
;

create index auth_user_username_6821ab7c_like
    on auth_user (username)
;

create table auth_user_groups
(
    id serial not null
        constraint auth_user_groups_pkey
            primary key,
    user_id integer not null
        constraint auth_user_groups_user_id_6a12ed8b_fk_auth_user_id
            references auth_user
                deferrable initially deferred,
    group_id integer not null
        constraint auth_user_groups_group_id_97559544_fk_auth_group_id
            references auth_group
                deferrable initially deferred,
    constraint auth_user_groups_user_id_group_id_94350c0c_uniq
        unique (user_id, group_id)
)
;

create index auth_user_groups_user_id_6a12ed8b
    on auth_user_groups (user_id)
;

create index auth_user_groups_group_id_97559544
    on auth_user_groups (group_id)
;

create table auth_user_user_permissions
(
    id serial not null
        constraint auth_user_user_permissions_pkey
            primary key,
    user_id integer not null
        constraint auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id
            references auth_user
                deferrable initially deferred,
    permission_id integer not null
        constraint auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm
            references auth_permission
                deferrable initially deferred,
    constraint auth_user_user_permissions_user_id_permission_id_14a6b632_uniq
        unique (user_id, permission_id)
)
;

create index auth_user_user_permissions_user_id_a95ead1b
    on auth_user_user_permissions (user_id)
;

create index auth_user_user_permissions_permission_id_1fbb5f2c
    on auth_user_user_permissions (permission_id)
;

create table django_admin_log
(
    id serial not null
        constraint django_admin_log_pkey
            primary key,
    action_time timestamp with time zone not null,
    object_id text,
    object_repr varchar(200) not null,
    action_flag smallint not null
        constraint django_admin_log_action_flag_check
            check (action_flag >= 0),
    change_message text not null,
    content_type_id integer
        constraint django_admin_log_content_type_id_c4bce8eb_fk_django_co
            references django_content_type
                deferrable initially deferred,
    user_id integer not null
        constraint django_admin_log_user_id_c564eba6_fk_auth_user_id
            references auth_user
                deferrable initially deferred
)
;

create index django_admin_log_content_type_id_c4bce8eb
    on django_admin_log (content_type_id)
;

create index django_admin_log_user_id_c564eba6
    on django_admin_log (user_id)
;

create table django_session
(
    session_key varchar(40) not null
        constraint django_session_pkey
            primary key,
    session_data text not null,
    expire_date timestamp with time zone not null
)
;

create index django_session_session_key_c0390e0f_like
    on django_session (session_key)
;

create index django_session_expire_date_a5c62663
    on django_session (expire_date)
;

create table revelation_project
(
    name varchar(100) not null
        constraint revelation_project_pkey
            primary key
)
;

create table revelation_modelinput
(
    id serial not null
        constraint revelation_modelinput_pkey
            primary key,
    name varchar(200) not null,
    project_id varchar(100)
        constraint revelation_modelinpu_project_id_1578a92e_fk_revelatio
            references revelation_project
                deferrable initially deferred,
    muted boolean not null,
    run_live boolean not null,
    constraint revelation_modelinput_name_project_id_4526dfaf_uniq
        unique (name, project_id)
)
;

create index revelation_modelinput_project_id_1578a92e
    on revelation_modelinput (project_id)
;

create index revelation_modelinput_project_id_1578a92e_like
    on revelation_modelinput (project_id)
;

create table revelation_modeltype
(
    id serial not null
        constraint revelation_modeltype_pkey
            primary key,
    name varchar(100) not null,
    model_input_id integer
        constraint revelation_modeltype_model_input_id_45eb6f49_fk_revelatio
            references revelation_modelinput
                deferrable initially deferred,
    project_id varchar(100)
        constraint revelation_modeltype_project_id_13065e76_fk_revelatio
            references revelation_project
                deferrable initially deferred,
    constraint revelation_modeltype_name_project_id_model_in_f4800b6c_uniq
        unique (name, project_id, model_input_id)
)
;

create table revelation_modelrun
(
    id serial not null
        constraint revelation_modelrun_pkey
            primary key,
    run_date timestamp with time zone not null,
    model_input_id integer
        constraint revelation_modelrun_model_input_id_cc2a1ab5_fk_revelatio
            references revelation_modelinput
                deferrable initially deferred,
    model_type_id integer
        constraint revelation_modelrun_model_type_id_22d3fa2f_fk_revelatio
            references revelation_modeltype
                deferrable initially deferred,
    project_id varchar(100)
        constraint revelation_modelrun_project_id_47a20e29_fk_revelatio
            references revelation_project
                deferrable initially deferred,
    constraint revelation_modelrun_project_id_model_input_i_8636802a_uniq
        unique (project_id, model_input_id, model_type_id, run_date)
)
;

create index revelation_modelrun_model_input_id_cc2a1ab5
    on revelation_modelrun (model_input_id)
;

create index revelation_modelrun_model_type_id_22d3fa2f
    on revelation_modelrun (model_type_id)
;

create index revelation_modelrun_project_id_47a20e29
    on revelation_modelrun (project_id)
;

create index revelation_modelrun_project_id_47a20e29_like
    on revelation_modelrun (project_id)
;

create index revelation_modeltype_model_input_id_45eb6f49
    on revelation_modeltype (model_input_id)
;

create index revelation_modeltype_project_id_13065e76
    on revelation_modeltype (project_id)
;

create index revelation_modeltype_project_id_13065e76_like
    on revelation_modeltype (project_id)
;

create table revelation_optimizationresult
(
    id serial not null
        constraint revelation_optimizationresults_pkey
            primary key,
    max_iterations integer not null,
    max_seconds integer not null,
    iterations integer not null,
    seconds integer not null,
    best_score double precision not null,
    best_params jsonb not null,
    scores double precision[] not null,
    model_run_id integer not null
        constraint revelation_optimizationresult_model_run_id_b2dca595_uniq
            unique
        constraint revelation_optimizat_model_run_id_b2dca595_fk_revelatio
            references revelation_modelrun
                deferrable initially deferred,
    score_type varchar(50),
    marked_as_best timestamp with time zone,
    version varchar(50) not null
)
;

create index revelation_project_project_361a0da1_like
    on revelation_project (name)
;

create table revelation_inputdata
(
    id serial not null
        constraint revelation_inputdata_pkey
            primary key,
    name varchar(100) not null,
    created_date timestamp with time zone not null,
    modified_date timestamp with time zone not null,
    model_input_id integer
        constraint revelation_inputdata_model_input_id_f071d047_fk_revelatio
            references revelation_modelinput
                deferrable initially deferred,
    extension varchar(20),
    delimiter varchar(5),
    file_size bigint,
    header_cols varchar(100) [],
    num_rows bigint,
    constraint revelation_inputdata_model_input_id_name_exte_eb8d8b28_uniq
        unique (model_input_id, name, extension)
)
;

create index revelation_inputdata_model_input_id_f071d047
    on revelation_inputdata (model_input_id)
;

create table revelation_pickledmodel
(
    id serial not null
        constraint revelation_pickledmodel_pkey
            primary key,
    name varchar(100) not null,
    extension varchar(20),
    created_date timestamp with time zone not null,
    modified_date timestamp with time zone not null,
    model_type_id integer
        constraint revelation_pickledmo_model_type_id_30487b2d_fk_revelatio
            references revelation_modeltype
                deferrable initially deferred,
    version varchar(50) not null,
    constraint revelation_pickledmodel_model_type_id_name_exten_31f65c91_uniq
        unique (model_type_id, name, extension, version)
)
;

create index revelation_pickledmodel_model_type_id_30487b2d
    on revelation_pickledmodel (model_type_id)
;

create table revelation_resultsdata
(
    id serial not null
        constraint revelation_resultsdata_pkey
            primary key,
    name varchar(100) not null,
    extension varchar(20),
    created_date timestamp with time zone not null,
    modified_date timestamp with time zone not null,
    model_run_id integer
        constraint revelation_resultsda_model_run_id_6405366c_fk_revelatio
            references revelation_modelrun
                deferrable initially deferred,
    constraint revelation_resultsdata_model_run_id_name_extens_ae7d74b5_uniq
        unique (model_run_id, name, extension)
)
;

create table revelation_facility
(
    id serial not null
        constraint revelation_facility_pkey
            primary key,
    facility_code varchar(30) not null,
    description varchar(100) not null
)
;

create table revelation_gpequipment
(
    modelinput_ptr_id integer not null
        constraint revelation_gpequipment_pkey
            primary key
        constraint revelation_gpequipme_modelinput_ptr_id_ebd82fae_fk_revelatio
            references revelation_modelinput
                deferrable initially deferred,
    facility_id integer not null
        constraint revelation_gpequipme_facility_id_b34c2b30_fk_revelatio
            references revelation_facility
                deferrable initially deferred,
    equipment_number varchar(40),
    type_description varchar(80),
    equipment_description varchar(150),
    location varchar(50),
    location_sub_1 varchar(40),
    location_sub_2 varchar(40),
    location_sub_3 varchar(40),
    model_number varchar(80),
    equipment_manufacturer varchar(50),
    parent_equipment_id integer
        constraint revelation_gpequipme_parent_equipment_id_fd1e3fa2_fk_revelatio
            references revelation_gpequipment
                deferrable initially deferred,
    kcf_wo_equipment_id integer
        constraint revelation_gpequipme_kcf_wo_equipment_id_315820e7_fk_revelatio
            references revelation_modelinput
                deferrable initially deferred
)
;

create index revelation_gpequipment_parent_equipment_id_fd1e3fa2
    on revelation_gpequipment (parent_equipment_id)
;

create index revelation_gpequipment_facility_code_id_b3a88907
    on revelation_gpequipment (facility_id)
;

create index revelation_gpequipment_kcf_wo_equipment_id_315820e7
    on revelation_gpequipment (kcf_wo_equipment_id)
;

create table revelation_gpequipment_child_equipment
(
    id serial not null
        constraint revelation_gpequipment_child_equipment_pkey
            primary key,
    from_gpequipment_id integer not null
        constraint revelation_gpequipme_from_gpequipment_id_3536f1c2_fk_revelatio
            references revelation_gpequipment
                deferrable initially deferred,
    to_gpequipment_id integer not null
        constraint revelation_gpequipme_to_gpequipment_id_7cf06ebd_fk_revelatio
            references revelation_gpequipment
                deferrable initially deferred,
    constraint revelation_gpequipment_c_from_gpequipment_id_to_g_c49c4872_uniq
        unique (from_gpequipment_id, to_gpequipment_id)
)
;

create index revelation_gpequipment_chi_from_gpequipment_id_3536f1c2
    on revelation_gpequipment_child_equipment (from_gpequipment_id)
;

create index revelation_gpequipment_chi_to_gpequipment_id_7cf06ebd
    on revelation_gpequipment_child_equipment (to_gpequipment_id)
;

create table revelation_integritycheck
(
    id serial not null
        constraint revelation_integritycheck_pkey
            primary key,
    run_date timestamp with time zone not null,
    name varchar(100),
    passed boolean not null,
    data jsonb not null,
    model_input_id integer
        constraint revelation_integrity_model_input_id_a4cae6fd_fk_revelatio
            references revelation_modelinput
                deferrable initially deferred,
    project_id varchar(100)
        constraint revelation_integrity_project_id_faa6eb21_fk_revelatio
            references revelation_project
                deferrable initially deferred
)
;

create index revelation_integritycheck_model_input_id_a4cae6fd
    on revelation_integritycheck (model_input_id)
;

create index revelation_integritycheck_project_id_faa6eb21
    on revelation_integritycheck (project_id)
;

create index revelation_integritycheck_project_id_faa6eb21_like
    on revelation_integritycheck (project_id)
;

create table revelation_preprocessingresult
(
    id serial not null
        constraint revelation_preprocessingresult_pkey
            primary key,
    max_iterations integer not null,
    max_seconds integer not null,
    iterations integer not null,
    seconds integer not null,
    best_score double precision not null,
    best_params jsonb not null,
    score_type varchar(50),
    scores double precision[] not null,
    run_date timestamp with time zone not null,
    model_input_id integer not null
        constraint revelation_preprocessingresult_model_input_id_key
            unique
        constraint revelation_preproces_model_input_id_3b3594c4_fk_revelatio
            references revelation_modelinput
                deferrable initially deferred
)
;

create table revelation_scorereport
(
    id serial not null
        constraint revelation_scorereport_pkey
            primary key,
    name varchar(100),
    score double precision not null,
    model_run_id integer
        constraint revelation_scorerepo_model_run_id_1138eeca_fk_revelatio
            references revelation_modelrun
                deferrable initially deferred
)
;

create index revelation_scorereport_model_run_id_1138eeca
    on revelation_scorereport (model_run_id)
;

create table revelation_batchrunresult
(
    id serial not null
        constraint revelation_batchrunresult_pkey
            primary key,
    run_date timestamp with time zone not null,
    model_type_id integer not null
        constraint revelation_batchrunr_model_type_id_fbd82636_fk_revelatio
            references revelation_modeltype
                deferrable initially deferred
)
;

create index revelation_batchrunresult_model_type_id_fbd82636
    on revelation_batchrunresult (model_type_id)
;

create table revelation_gphierarchynode
(
    id serial not null
        constraint revelation_gphierarchynode_pkey
            primary key,
    name varchar(100) not null,
    lft integer not null
        constraint revelation_gphierarchynode_lft_check
            check (lft >= 0),
    rght integer not null
        constraint revelation_gphierarchynode_rght_check
            check (rght >= 0),
    tree_id integer not null
        constraint revelation_gphierarchynode_tree_id_check
            check (tree_id >= 0),
    level integer not null
        constraint revelation_gphierarchynode_level_check
            check (level >= 0),
    equipment_id integer
        constraint revelation_gphierarchynode_equipment_id_key
            unique
        constraint revelation_gphierarc_equipment_id_13625694_fk_revelatio
            references revelation_gpequipment
                deferrable initially deferred,
    parent_id integer
        constraint revelation_gphierarc_parent_id_ea9d900e_fk_revelatio
            references revelation_gphierarchynode
                deferrable initially deferred,
    equipment_number varchar(40),
    is_variable boolean not null
)
;

create index revelation_gphierarchynode_lft_8c4b1b5a
    on revelation_gphierarchynode (lft)
;

create index revelation_gphierarchynode_rght_22c24d84
    on revelation_gphierarchynode (rght)
;

create index revelation_gphierarchynode_tree_id_355a3f01
    on revelation_gphierarchynode (tree_id)
;

create index revelation_gphierarchynode_level_b0c43659
    on revelation_gphierarchynode (level)
;

create index revelation_gphierarchynode_parent_id_ea9d900e
    on revelation_gphierarchynode (parent_id)
;

create table revelation_chiresult
(
    id serial not null
        constraint revelation_chiresult_pkey
            primary key,
    name varchar(100) not null,
    extension varchar(20),
    created_date timestamp with time zone not null,
    modified_date timestamp with time zone not null,
    model_input_id integer not null
        constraint revelation_chiresult_model_input_id_6d042988_fk_revelatio
            references revelation_modelinput
                deferrable initially deferred
)
;

create index revelation_chiresult_model_input_id_6d042988
    on revelation_chiresult (model_input_id)
;

create table revelation_maintenancedata
(
    id serial not null
        constraint revelation_maintenancedata_pkey
            primary key,
    work_order_number varchar(20),
    start_datetime timestamp with time zone not null,
    end_datetime timestamp with time zone not null,
    work_order_description varchar(200),
    type_description varchar(100),
    type_sub_description varchar(50),
    parent_work_order_number varchar(20),
    actual_duration double precision,
    type varchar(20) not null,
    equipment_id integer
        constraint revelation_maintenan_equipment_id_27dcad47_fk_revelatio
            references revelation_gpequipment
                deferrable initially deferred,
    facility_id integer
        constraint revelation_maintenan_facility_id_dc088756_fk_revelatio
            references revelation_facility
                deferrable initially deferred,
    date_added date,
    constraint revelation_maintenanceda_equipment_id_work_order__fbff5216_uniq
        unique (equipment_id, work_order_number)
)
;

create index revelation_maintenancedata_equipment_id_27dcad47
    on revelation_maintenancedata (equipment_id)
;

create index revelation_maintenancedata_facility_id_dc088756
    on revelation_maintenancedata (facility_id)
;

create table revelation_modelcolumn
(
    id serial not null
        constraint revelation_modelcolumn_pkey
            primary key,
    name varchar(500) not null,
    index integer not null,
    version varchar(50) not null,
    model_input_id integer not null
        constraint revelation_modelcolu_model_input_id_ef15cc7b_fk_revelatio
            references revelation_modelinput
                deferrable initially deferred,
    model_type_id integer
        constraint revelation_modelcolu_model_type_id_c8146c1c_fk_revelatio
            references revelation_modeltype
                deferrable initially deferred,
    live_data_name varchar(500),
    ignore_live boolean not null,
    constraint revelation_modelcolumn_model_input_id_model_typ_846c1d78_uniq
        unique (model_input_id, model_type_id, name, index, version)
)
;

create index revelation_modelcolumn_model_input_id_ef15cc7b
    on revelation_modelcolumn (model_input_id)
;

create index revelation_modelcolumn_model_type_id_c8146c1c
    on revelation_modelcolumn (model_type_id)
;

create table revelation_chidata
(
    id serial not null
        constraint revelation_chidata_pkey
            primary key,
    timestamp bigint not null,
    model_type_name varchar(100) not null,
    version varchar(50),
    is_final_output boolean not null,
    output double precision[] not null,
    model_input_id integer
        constraint revelation_chidata_model_input_id_d9152832_fk_revelatio
            references revelation_modelinput
                deferrable initially deferred,
    constraint revelation_chidata_model_input_id_timestamp_67cc53de_uniq
        unique (model_input_id, timestamp, model_type_name, version)
)
;

create index revelation_chidata_model_input_id_d9152832
    on revelation_chidata (model_input_id)
;

create table revelation_liveinputdata
(
    id serial not null
        constraint revelation_liveinputdata_pkey
            primary key,
    name varchar(100) not null,
    extension varchar(20),
    created_date timestamp with time zone not null,
    modified_date timestamp with time zone not null,
    timestamp bigint not null,
    model_input_id integer
        constraint revelation_liveinput_model_input_id_8312c2de_fk_revelatio
            references revelation_modelinput
                deferrable initially deferred,
    constraint revelation_liveinputdata_model_input_id_timestamp_f812671e_uniq
        unique (model_input_id, timestamp)
)
;

create index revelation_liveinputdata_model_input_id_8312c2de
    on revelation_liveinputdata (model_input_id)
;

create table revelation_livedatatimediffstat
(
    id serial not null
        constraint revelation_livedatatimediffstat_pkey
            primary key,
    value double precision not null,
    timestamp timestamp with time zone not null,
    "group" varchar(100),
    constraint revelation_livedatatimediffstat_group_timestamp_9ce92336_uniq
        unique ("group", timestamp)
)
;

create table revelation_flagevent
(
    id serial not null
        constraint revelation_flagevent_pkey
            primary key,
    timestamp bigint not null,
    version varchar(50) not null,
    model_input_id integer not null
        constraint revelation_flagevent_model_input_id_edb56a86_fk_revelatio
            references revelation_modelinput
                deferrable initially deferred,
    approved boolean not null,
    description varchar(500)
)
;

create index revelation_flagevent_model_input_id_edb56a86
    on revelation_flagevent (model_input_id)
;

create table revelation_versionconfig
(
    id serial not null
        constraint revelation_versionconfig_pkey
            primary key,
    environment varchar(50) not null,
    version varchar(50) not null,
    model_input_id integer
        constraint revelation_versionco_model_input_id_a1980574_fk_revelatio
            references revelation_modelinput
                deferrable initially deferred,
    model_type_id integer
        constraint revelation_versionco_model_type_id_64f8ba21_fk_revelatio
            references revelation_modeltype
                deferrable initially deferred
)
;

create index revelation_versionconfig_model_input_id_a1980574
    on revelation_versionconfig (model_input_id)
;

create index revelation_versionconfig_model_type_id_64f8ba21
    on revelation_versionconfig (model_type_id)
;

create table revelation_outputscaler
(
    id serial not null
        constraint revelation_outputscaler_pkey
            primary key,
    created_date timestamp with time zone not null,
    modified_date timestamp with time zone not null,
    name varchar(100) not null,
    extension varchar(20) not null,
    version varchar(50) not null,
    model_type_id integer
        constraint revelation_outputsca_model_type_id_9f49f5f4_fk_revelatio
            references revelation_modeltype
                deferrable initially deferred,
    constraint revelation_outputscaler_model_type_id_name_exten_237a3e77_uniq
        unique (model_type_id, name, extension, version)
)
;

create index revelation_outputscaler_model_type_id_9f49f5f4
    on revelation_outputscaler (model_type_id)
;

create table revelation_modelpredictiondata
(
    id serial not null
        constraint revelation_modelpredictiondata_pkey
            primary key,
    created_date timestamp with time zone not null,
    modified_date timestamp with time zone not null,
    name varchar(100) not null,
    extension varchar(20) not null,
    version varchar(50) not null,
    training_preds boolean not null,
    model_type_id integer
        constraint revelation_modelpred_model_type_id_e9705c63_fk_revelatio
            references revelation_modeltype
                deferrable initially deferred,
    constraint revelation_modelpredicti_model_type_id_name_exten_87bbf5a1_uniq
        unique (model_type_id, name, extension, version)
)
;

create index revelation_modelpredictiondata_model_type_id_e9705c63
    on revelation_modelpredictiondata (model_type_id)
;

