FROM python:3.6
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

WORKDIR /srv/celeri
ADD ./celeri /srv/celeri
RUN cd /srv/celeri/
RUN pip install -r requirements.txt
RUN pip install gunicorn

EXPOSE 8000

CMD ./entrypoint.sh