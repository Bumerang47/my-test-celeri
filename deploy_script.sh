#!/bin/bash

ls
pip install pip --upgrade
pip install -r /var/app/current/celeri/requirements.txt

python /var/app/current/celeri/manage.py makemigrations
python /var/app/current/celeri/manage.py migrate
python /var/app/current/celeri/manage.py runsslserver 0.0.0.0:8000
