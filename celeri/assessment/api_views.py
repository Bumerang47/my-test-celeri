import pytz

from django.core.urlresolvers import reverse
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from rest_framework.views import APIView
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response

from .models import Assessment
from .serializers import AssessmentSerializer, PatientSerializer
from .views import create_assessment, get_oauth


@csrf_exempt
def assessment_link(request, patient_id):
    """
    List all code snippets, or create a new snippet.
    """
    if request.method == 'GET':

        snippets = Assessment.objects.all()
        serializer = AssessmentSerializer(snippets, many=True)
        return JsonResponse(serializer.data, safe=False)

    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = AssessmentSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201)
        return JsonResponse(serializer.errors, status=400)


class RemoteAssessment(APIView):
    """
    This endpoint is used to generate a URL so a patient can take an assessment outside of the clinic

    """

    def get(self, request, patient_id, user_id, format=None):
        screening = {
            'title': 'PROMIS-29',
            'instruments': [
                {
                    'name': 'Pain Interference',
                    'oid': '154D0273-C3F6-4BCE-8885-3194D4CC4596'
                },
                {
                    'name': 'Ability to Participate Social',
                    'oid': '5211C7AF-8F4A-4648-90A6-ECE5669AE0EF'
                },
                {
                    'name': 'Sleep Disturbance',
                    'oid': '795B07C1-067E-4FBD-9B60-A57985E69B5D'
                },
                {
                    'name': 'Fatigue',
                    'oid': 'B3B30AF1-7536-451D-AD39-62A097A5EA4D'
                },
                {
                    'name': 'Depression',
                    'oid': '597A9B24-5B5C-487D-9606-451355DC6E3D'
                },
                {
                    'name': 'Anxiety',
                    'oid': 'FFCDF6E3-8B17-4673-AB38-C677FFF6DBAF'
                },
                {
                    'name': 'Physical Function',
                    'oid': '80C5D4A3-FC1F-4C1B-B07E-10B796CF8105'
                }
            ]
        }

        import uuid
        uuid = str(uuid.uuid1())
        assessment = create_assessment(patient_id=patient_id,
                                       timezone=pytz.UTC,
                                       physician_id=user_id,
                                       screening=screening,
                                       type=Assessment.DIAGNOSTIC,
                                       uuid=uuid)

        relative_url = reverse('start_remote_assessment', kwargs={'assessment_uuid': assessment.uuid})
        full_url = request.build_absolute_uri(relative_url)
        return JsonResponse({'url': full_url})


class PatientsList(GenericAPIView):
    queryset = Assessment.objects.all()

    def get(self, request):
        filter_text = request.GET.get('filter', '')
        page = request.GET.get('page', 1)

        url_search = 'https://celerihealth.com/api/patients'
        params = {
            'search': filter_text,
            'orderBy': 'created_at',
            'sortedBy': 'desc',
            'page': page
        }
        success, json_data = get_oauth(url_search, request, params=params)
        serializer = PatientSerializer(json_data.get('data'), many=True)

        page_data = {
            'data': serializer.data,
            'current_page': json_data.get('current_page'),
            'last_page': json_data.get('last_page'),
        }
        return Response(page_data)


class PercentComplete(APIView):
    queryset = Assessment.objects.all()

    def get(self, request, assessment_uuid):
        assessment = get_object_or_404(Assessment, uuid=assessment_uuid)
        instruments = assessment.instrument_set.all()
        total = 0
        complete = 0
        for instrument in instruments:
            total += 1
            if instrument.datetime_finished is not None:
                complete += 1
        return JsonResponse({'uuid': assessment_uuid, 'percent_complete': float(complete/total)})
