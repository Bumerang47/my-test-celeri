from django.conf.urls import url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from . import views

urlpatterns = [
    url(r'^$', views.patient_index, name='index'),
    url(r'^patients$', views.patient_index, name='patient_index'),
    url(r'^battery_index', views.battery_index, name='battery_index'),
    url(r'^new_patient$', views.new_patient, name='new_patient'),
    url(r'^patient_detail/(?P<patient_id>[0-9]+)$', views.patient_detail, name='patient_detail'),
    url(r'^start_assessment/(?P<assessment_id>[0-9]+)$', views.start_assessment, name='start_assessment'),
    url(r'^take_assessment/(?P<assessment_id>[0-9]+)$', views.take_assessment, name='take_assessment'),
    url(r'^finished_assessment/(?P<assessment_id>[0-9]+)$', views.finished_assessment, name='finished_assessment'),
    url(
        r'^remote_assessment/(?P<assessment_uuid>[0-9a-f-]+)$',
        views.start_remote_assessment,
        name='start_remote_assessment'
    ),
    url(r'^create_pin$', views.create_pin, name='create_pin'),
    url(r'^enter_pin$', views.enter_pin, name='enter_pin'),
    url(r'^thank_you/(?P<assessment_id>[0-9]+)$', views.thank_you, name='thank_you'),
    url(r'^ort/(?P<instrument_id>[0-9]+)$', views.ort, name='ort')
]

urlpatterns += staticfiles_urlpatterns()
