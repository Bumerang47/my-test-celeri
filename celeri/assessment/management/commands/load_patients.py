import pandas as pd
from django.core.management.base import BaseCommand
from requests_oauthlib import OAuth2Session
from requests.exceptions import RequestException
from oauthlib.oauth2 import LegacyApplicationClient
from oauthlib.oauth2.rfc6749.errors import MissingTokenError


token_url = 'https://celerihealth.com/oauth/token'


CLIENT_ID = '2'
CLIENT_SECRET = 'ayHLMR205O2DmVBPIuUSevshNGQZiKwiwW8BmjjX'

COL_MRN = 'Pat #'
COL_FIRST_NAME = 'Pat F Name'
COL_LAST_NAME = 'Pat L Name'
COL_SEX = 'Pat Sex'
COL_DOB = 'Pat Birthdate'


def post_oauth(url, post_data, token):

    oauth = OAuth2Session(CLIENT_ID, token=token)
    try:
        oauth_response = oauth.post(url=url, data=post_data)
    except RequestException:
        return False, None

    if oauth_response.status_code != 200:
        return False, None

    # API returns HTML response if token is no longer valid, so return login page
    if 'application/json' not in oauth_response.headers['content-type']:
        return False, None

    return True, oauth_response


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('username', help='Username (email)')
        parser.add_argument('password', help='Password')
        parser.add_argument('file_path', help='Path of the excel file')

    def handle(self, *args, **options):
        username = options['username']
        password = options['password']
        file_path = options['file_path']

        oauth = OAuth2Session(client=LegacyApplicationClient(client_id=CLIENT_ID))
        try:
            token = oauth.fetch_token(token_url=token_url,
                                      username=username, password=password, client_id=CLIENT_ID,
                                      client_secret=CLIENT_SECRET)
        except MissingTokenError:
            # invalid credentials
            return None

        xl = pd.ExcelFile(file_path)
        first_sheet = xl.sheet_names[0]
        df = xl.parse(first_sheet)

        for index, row in df.iterrows():
            mrn = row[COL_MRN]
            first_name = row[COL_FIRST_NAME]
            last_name = row[COL_LAST_NAME]
            gender = row[COL_SEX]
            dob = row[COL_DOB]

            try:
                post_data = {
                    "name": f'{first_name} {last_name}',
                    "mrn": mrn,
                    "dob": dob.strftime('%Y-%m-%d'),
                    "gender": gender
                }
            except AttributeError:
                print(f'Unable to get dob {dob}')
                continue

            success, response = post_oauth('https://celerihealth.com/api/patients', post_data, token)
            print(post_data['name'])
            print(response)
