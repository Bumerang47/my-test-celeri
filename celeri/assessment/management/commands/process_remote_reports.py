from django.core.management.base import BaseCommand
from requests_oauthlib import OAuth2Session
from requests.exceptions import RequestException
from oauthlib.oauth2 import LegacyApplicationClient
from oauthlib.oauth2.rfc6749.errors import MissingTokenError
from assessment.views import create_and_post_results
from assessment.models import Assessment


token_url = 'https://celerihealth.com/oauth/token'


CLIENT_ID = '2'
CLIENT_SECRET = 'ayHLMR205O2DmVBPIuUSevshNGQZiKwiwW8BmjjX'


def post_oauth(url, post_data, token):

    oauth = OAuth2Session(CLIENT_ID, token=token)
    try:
        oauth_response = oauth.post(url=url, data=post_data)
    except RequestException:
        return False, None

    if oauth_response.status_code != 200:
        return False, None

    # API returns HTML response if token is no longer valid, so return login page
    if 'application/json' not in oauth_response.headers['content-type']:
        return False, None

    return True, oauth_response


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('username', help='Username (email)')
        parser.add_argument('password', help='Password')
        parser.add_argument('file_path', help='Path of the excel file')

    def handle(self, *args, **options):
        username = options['username']
        password = options['password']

        oauth = OAuth2Session(client=LegacyApplicationClient(client_id=CLIENT_ID))
        try:
            oauth.fetch_token(
                token_url=token_url,
                username=username,
                password=password,
                client_id=CLIENT_ID,
                client_secret=CLIENT_SECRET
            )
        except MissingTokenError:
            # invalid credentials
            return None

        unreported_assessments = Assessment.objects.filter(report_id__isnull=True)
        for assessment in unreported_assessments:
            create_and_post_results(assessment, None)
