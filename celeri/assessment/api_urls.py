from django.conf.urls import url

from . import api_views

urlpatterns = [
    url(r'^remote_assessment/(?P<patient_id>\d+)/(?P<user_id>\d+)$', api_views.RemoteAssessment.as_view(), name='remote_assessment'),
    url(r'^patients/$', api_views.PatientsList.as_view(), name='get_patients'),
    url(r'^percent_complete/(?P<assessment_uuid>[0-9a-f-]+)/$', api_views.PercentComplete.as_view(), name='percent_complete'),
]
