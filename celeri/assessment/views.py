import json
import logging
import math
import pandas as pd
import pytz
import requests
from collections import defaultdict
from datetime import datetime
from dateutil.parser import parse
from django.contrib import messages
from django.contrib.auth.models import AnonymousUser
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import LoginView as LoginViewContrib
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.utils import timezone as django_timezone
from django.utils.html import strip_tags
from requests.auth import HTTPBasicAuth
from requests.exceptions import RequestException
from requests_oauthlib import OAuth2Session

from .serializers import PatientSerializer
from .models import (
    Assessment,
    Instrument,
    CustomInstrumentInstance,
    CustomInstrumentDefinition,
    InstrumentQuestionInstance,
    InstrumentChoice,
    TokenData
)
from .forms import (
    NewPatientForm,
    OpioidRiskForm,
    CreatePinForm,
    EnterPinForm,
    PROMIS29_OID,
    AuthenticationForm,
)

API_HOST = '54.172.126.212'
REGISTER_INSTRUMENT_URL = 'http://{API_HOST}/ac_api/2014-01/Assessments/{form_oid}.json'

logger = logging.getLogger(__name__)
REGISTRATION_OID = "D163D41E-F75E-4BC2-A95A-C6BDF62E4179"
TOKEN = '27FB910E-6849-4DAA-90EC-53D136E5E856'

KEY_PIN_START = 'pin_session_start'
KEY_PIN_SESSION_VALID = 'pin_session_valid'
KEY_CURRENT_ASSESSMENT_ID = 'current_assessment_id'
KEY_STARTING_ASSESSMENT = 'starting_assessment'

CLIENT_ID = '2'
CLIENT_SECRET = 'ayHLMR205O2DmVBPIuUSevshNGQZiKwiwW8BmjjX'
token_url = 'https://celerihealth.com/oauth/token'

recommendation_db = {
    "Pain Intensity": "Will continue to diagnose and treat patient's pain problem.",
    "Pain Interference": "Will continue to diagnose and treat patient's pain problem.",
    "Anger": "Patient is recommended to practice mindfulness meditation.",
    "Anxiety": (
        "Patient may benefit from dual use medication for pain and mood "
        "and employing non-pharmacologic strategies such as mindfulness "
        "meditation and/or CBT. Consider Psychiatry referral."
    ),
    "Physical Function": (
        "Patient will benefit from improvement in physical function through "
        "structured activity, including physical therapy and personalized "
        "home exercise program."
    ),
    "Emotional Support": (
        "Patient will benefit from group therapy to foster a support system "
        "and appropriate coping strategies."
    ),
    "Social Isolation": (
        "Patient will benefit from group therapy to foster a support system "
        "and appropriate coping strategies."
    ),
    "Satisfaction Roles Activities": (
        "Patient will benefit from improvement in physical function through "
        "structured activity, including physical therapy and personalized "
        "home exercise program."
    ),
    "Depression": (
        "Patient may benefit from dual use medication for pain and mood and "
        "employing non-pharmacologic strategies such as mindfulness meditation "
        "and/or CBT. Consider Psychiatry referral."
    ),
    "Sleep Disturbance": ""
}

educational_content_db = {

}

# TODO: model these and put in DB
screening_choices = [
    {
        'id': 1,
        'title': 'Medication Screening',
        'instruments': [
            {
                'name': 'Opioid Risk',
                'oid': '',
                'ort': True
            },
            {
                'name': 'Self-efficacy Manage Meds/TX',
                'oid': '856F5AFB-30F2-436B-817A-797C0FA96F14'
            },
            {
                'name': 'Alcohol Use',
                'oid': '2732FAC1-6C15-4882-AAA1-28D23DA57FB0'
            },
            {
                'oid': '154D0273-C3F6-4BCE-8885-3194D4CC4596',
                'name': 'Pain Interference'
            }
        ]
    },
    {
        'id': 2,
        'title': 'Emotional Wellbeing Screening',
        'instruments': [
            {
                'oid': '154D0273-C3F6-4BCE-8885-3194D4CC4596',
                'name': 'Pain Interference'
            },
            {
                'oid': 'FFCDF6E3-8B17-4673-AB38-C677FFF6DBAF',
                'name': 'Anxiety'
            },
            {
                'oid': '597A9B24-5B5C-487D-9606-451355DC6E3D',
                'name': 'Depression'
            },
            {
                'oid': '795B07C1-067E-4FBD-9B60-A57985E69B5D',
                'name': 'Sleep Disturbance'
            }
        ]
    },
    {
        'id': 3,
        'title': 'Functional Screening',
        'instruments': [
            {
                'oid': '80C5D4A3-FC1F-4C1B-B07E-10B796CF8105',
                'name': 'Physical Function'
            },
            {
                'oid': '36F00430-CC4C-4977-AE8A-0787B3C53AB8',
                'name': 'Social Sat Role'
            },
            {
                'oid': '5211C7AF-8F4A-4648-90A6-ECE5669AE0EF',
                'name': 'Ability to Participate'
            },
            {
                'oid': '154D0273-C3F6-4BCE-8885-3194D4CC4596',
                'name': 'Pain Interference'
            }
        ]
    },
    {
        'id': 4,
        'title': 'Pain Screening',
        'instruments': [
            {
                'oid': '7224FB55-81D7-4131-A136-8C89907CDFBB',
                'name': 'Pain Intensity'
            },
            {
                'oid': '80C5D4A3-FC1F-4C1B-B07E-10B796CF8105',
                'name': 'Physical Function'
            },
            {
                'oid': '795B07C1-067E-4FBD-9B60-A57985E69B5D',
                'name': 'Sleep Disturbance'
            },
            {
                'oid': '154D0273-C3F6-4BCE-8885-3194D4CC4596',
                'name': 'Pain Interference'
            }
        ]
    }
]


assessment_choices = [
    {
        'id': 5,
        'title': 'PROMIS-29',
        'instruments': [
            {
                'name': 'Pain Interference',
                'oid': '154D0273-C3F6-4BCE-8885-3194D4CC4596'
            },
            {
                'name': 'Ability to Participate Social',
                'oid': '5211C7AF-8F4A-4648-90A6-ECE5669AE0EF'
            },
            {
                'name': 'Sleep Disturbance',
                'oid': '795B07C1-067E-4FBD-9B60-A57985E69B5D'
            },
            {
                'name': 'Fatigue',
                'oid': 'B3B30AF1-7536-451D-AD39-62A097A5EA4D'
            },
            {
                'name': 'Depression',
                'oid': '597A9B24-5B5C-487D-9606-451355DC6E3D'
            },
            {
                'name': 'Anxiety',
                'oid': 'FFCDF6E3-8B17-4673-AB38-C677FFF6DBAF'
            },
            {
                'name': 'Physical Function',
                'oid': '80C5D4A3-FC1F-4C1B-B07E-10B796CF8105'
            }
        ]
    }
]


SUPPORT_MSG = 'Please contact support at support@celerihealth.com'


def get_oauth(url, request, params=None):
    if 'oauth_token' not in request.session:
        return False, render(request, 'registration/login.html')

    oauth = OAuth2Session(CLIENT_ID, token=request.session['oauth_token'])
    try:
        oauth_response = oauth.get(url=url, params=params)
    except RequestException:
        messages.error(request, SUPPORT_MSG)
        logger.error(f'Unable to get oauth response {url}')
        return False, None

    if oauth_response.status_code != 200:
        messages.error(request, SUPPORT_MSG)
        logger.error(f'Unable to get oauth response {url}')
        return False, None

    # API returns HTML response if token is no longer valid, so return login page
    if 'application/json' not in oauth_response.headers['content-type']:
        return False, render(request, 'registration/login.html')

    json_data = oauth_response.json()['data']
    return True, json_data


def post_oauth(url, post_data, request, oauth_token=None):
    if 'oauth_token' not in request.session:
        return False, render(request, 'registration/login.html')

    oauth = OAuth2Session(CLIENT_ID, token=request.session['oauth_token'])

    try:
        oauth_response = oauth.post(url=url, data=post_data)
    except RequestException:
        messages.error(request, SUPPORT_MSG)
        logger.error(f'Unable to get oauth response {url}')
        return False, None

    if oauth_response.status_code != 200:
        messages.error(request, SUPPORT_MSG)
        logger.error(f'Unable to get oauth response {url}')
        return False, None

    # API returns HTML response if token is no longer valid, so return login page
    if 'application/json' not in oauth_response.headers['content-type']:
        return False, render(request, 'registration/login.html')

    return True, oauth_response


# Create your views here.
@login_required
def index(request):
    return render(request, 'index.html', {})


def start_pin_session(request):
    request.session[KEY_PIN_START] = str(datetime.utcnow())
    request.session[KEY_PIN_SESSION_VALID] = True
    return request


@login_required
def create_pin(request):
    # TODO: check pin session cookie?
    # TODO: redirect to input PIN page first (assuming pin is set)

    if request.method == 'POST':
        form = CreatePinForm(request.POST)
        if form.is_valid():
            pin = form.cleaned_data['pin']

            oauth = OAuth2Session(CLIENT_ID, token=request.session['oauth_token'])
            data = {
                'pin': int(pin)
            }
            response = render(request, 'create_pin.html')
            try:
                oauth_response = oauth.post(url='https://celerihealth.com/api/pins', data=data)
            except RequestException:
                messages.error(request, 'Unable to create pin. Please contact support at support@celerihealth.com')
                return response

            if oauth_response.status_code != 200:
                messages.error(request, 'Unable to create pin. Please contact support at support@celerihealth.com')
                return response

            request.session['creating_pin'] = False
            messages.success(request, 'Passcode set')

            start_pin_session(request)
            return HttpResponseRedirect(reverse('patient_index'))
        else:
            messages.error(request, 'Please try again')
            return render(request, 'create_pin.html')
    else:
        return render(request, 'create_pin.html')


@login_required
def enter_pin(request):
    provider_logo_url, user_name, timezone = get_logo_user_name_and_timezone(request)

    if request.method == 'POST':
        form = EnterPinForm(request.POST)
        if form.is_valid():
            pin = form.cleaned_data['pin']

            success, json_data = get_oauth('https://celerihealth.com/api/pins', request)
            if not success:
                if json_data is None:
                    return render(request, 'enter_pin.html')
                else:
                    return json_data

            # TODO: if pin is incorrect 3 times in a row, logout the user with error message
            pin_data = json_data[0]
            backend_pin = pin_data['pin']

            try:
                pin = int(pin)
            except ValueError:
                messages.error(request, 'Invalid passcode')
                return render(request, 'enter_pin.html')

            if pin != backend_pin:
                messages.error(request, 'Invalid passcode')
                return render(request, 'enter_pin.html')

            # TODO: implement next url stuff
            if 'next_url' in request.session and request.session['next_url'] is not None:
                next_url = request.session['next_url']
            else:
                next_url = reverse('patient_index')

            request.session['next_url'] = None
            start_pin_session(request)
            return HttpResponseRedirect(next_url)
        else:
            messages.error(request, 'Invalid passcode')
            return render(request, 'enter_pin.html', {'user_name': user_name,
                                                      'provider_logo_url': provider_logo_url})
    else:
        return render(request, 'enter_pin.html', {'user_name': user_name,
                                                  'provider_logo_url': provider_logo_url})


@login_required
def patient_index(request):

    provider_logo_url, user_name, _ = get_logo_user_name_and_timezone(request)
    tz_name = django_timezone.get_current_timezone_name()

    today = datetime.now(pytz.timezone(tz_name)).strftime('%A, %B %-d, %Y')

    url_search = 'https://celerihealth.com/api/patients'
    params = {
        'orderBy': 'created_at',
        'sortedBy': 'desc',
        'page': 1
    }
    success, json_data = get_oauth(url_search, request, params=params)
    if not success:
        return json_data

    patient_list = json_data.get('data', [])
    patients_kit = PatientSerializer(patient_list, many=True)
    patients_data = patients_kit.data
    payload = {
        'patients': patients_data,
        'user_name': user_name,
        'provider_logo_url': provider_logo_url,
        'today': today,
        'timezone': tz_name,
        'paginator': {
            'current_page': json_data.get('current_page', 1),
            'last_page': json_data.get('last_page', 1),
            'page_range': range(1, json_data.get('last_page', 1) + 1)
        }
    }
    return render(request, 'patient_index.html', payload)


def get_logo_user_name_and_timezone(request):
    if 'provider_logo_url' in request.session and 'user_name' in request.session and 'timezone' in request.session:
        django_timezone.activate(request.session['timezone'])
        return request.session['provider_logo_url'], request.session['user_name'], request.session['timezone']

    success, json_data = get_oauth('https://celerihealth.com/api/info', request)

    user_name = ''
    timezone = 'UTC'
    provider_logo_url = ''
    if 'user' in json_data:
        user_data = json_data['user'][0]
        user_name = user_data['name']
        timezone = user_data['timezone'] if user_data['timezone'] else timezone
    if 'practice' in json_data:
        provider_data = json_data['practice'][0]
        provider_logo_url = provider_data['photo_url']

    request.session['provider_logo_url'] = provider_logo_url
    request.session['user_name'] = user_name
    request.session['timezone'] = timezone
    django_timezone.activate(request.session['timezone'])
    return provider_logo_url, user_name, timezone


def get_patient_info(request, patient_id):
    oauth = OAuth2Session(CLIENT_ID, token=request.session['oauth_token'])
    try:
        oauth_response = oauth.get(url=f'https://celerihealth.com/api/patients/{patient_id}')
    except Exception as e:
        logger.error('Unable to get patients', e)
        return None

    if oauth_response.status_code != 200:
        logger.error(request, 'Unable to get patients. Please contact support at support@celerihealth.com')
        return None

    json_data = json.loads(oauth_response.text)
    print(json_data)
    return json_data['data']


def get_physician_choices(request):
    success, json_data = get_oauth('https://celerihealth.com/api/physicians', request)
    return json_data


@login_required
def patient_detail(request, patient_id):
    provider_logo_url, user_name, timezone = get_logo_user_name_and_timezone(request)
    patient = get_patient_info(request, patient_id)
    assessments = Assessment.objects.filter(patient_id=patient_id).order_by('-date_created')

    physician_choices = get_physician_choices(request)

    if request.method == 'POST':
        screening_id = int(request.POST['screening_id'])
        physician_id = int(request.POST['physician_id'])
        assessment = None
        for screening in screening_choices:
            if screening_id == screening['id']:
                assessment = create_assessment(patient_id, timezone, physician_id, screening, Assessment.SCREENING)
        for choice in assessment_choices:
            if screening_id == choice['id']:
                assessment = create_assessment(patient_id, timezone, physician_id, choice, Assessment.DIAGNOSTIC)
        if assessment is None:
            messages.error(request, 'Unable to find assessment. Please try again.')

        return HttpResponseRedirect(reverse('start_assessment', kwargs={'assessment_id': assessment.id}))

    return render(request, 'patient_detail.html', {
        'patient_id': patient_id,
        'patient': patient,
        'assessments': assessments,
        'user_name': user_name,
        'provider_logo_url': provider_logo_url,
        'screening_choices': screening_choices,
        'assessment_choices': assessment_choices,
        'physician_choices': physician_choices
    })


def get_longitudinal_chart_data(assessment_history, instrument_names, current_assessment):
    asmt_data = []
    instmnt_data_dict = defaultdict(list)
    instmnt_diff_dict = dict()

    # get 4 instruments with biggest change
    for instrument_name in instrument_names:
        instruments = Instrument.objects.filter(
            name=instrument_name,
            assessment__in=assessment_history
        ).order_by('-datetime_finished')
        most_recent_instrument = instruments.first()

        max_instrument = instruments.order_by('-result_percentile').first()
        min_instrument = instruments.order_by('result_percentile').first()
        # some instruments might not have a result percentile, so skip
        if not most_recent_instrument.result_percentile or \
           not max_instrument.result_percentile or \
           not min_instrument.result_percentile:
            continue

        max_diff = abs(most_recent_instrument.result_percentile - max_instrument.result_percentile)
        min_diff = abs(most_recent_instrument.result_percentile - min_instrument.result_percentile)
        biggest_diff = max(max_diff, min_diff)
        instmnt_diff_dict[instrument_name] = biggest_diff

    # sort instruments by diff, select top 4
    sorted_instruments = sorted(instmnt_diff_dict.items(), key=lambda kv: kv[1], reverse=True)

    for (instrument_name, diff) in sorted_instruments[:4]:
        instruments = Instrument.objects.filter(
            name=instrument_name,
            assessment__in=assessment_history
        ).order_by('-datetime_finished')

        # don't show any longitudinal data if there's only one instrument in history
        if len(instruments) < 2:
            continue

        for instrument in instruments:
            # if (
            #   'Physical Function' in instrument.name
            #   or ('Participat' in instrument.name and 'Social' in instrument.name)
            # ):
            #     result_percentile = get_inverted_percentile(instrument)
            # else:
            result_percentile = instrument.result_percentile

            data_point = (instrument.datetime_finished.timestamp() * 1000, result_percentile)
            instmnt_data_dict[instrument.name].append(data_point)

    count = 0
    colors = [
        "#7cb5ec", "#434348", "#90ed7d", "#f7a35c", "#8085e9",
        "#f15c80", "#e4d354", "#2b908f", "#f45b5b", "#91e8e1"
    ]
    shapes = ["circle", "square", "diamond", "triangle", "triangle-down"]
    for key, value in instmnt_data_dict.items():
        # sort by date
        value.sort(key=lambda x: x[0])
        color_idx = count % len(colors)
        shape_idx = count % len(shapes)
        name = key

        asmt_data.append({
            'name': name,
            'data': value,
            'color': colors[color_idx],
            'symbol': shapes[shape_idx],
            'dataLabels': {
                'enabled': 'true',
                'format': '{x:%m/%d/%y}'
            }
        })
        count += 1
    return asmt_data


def get_inverted_percentile(instrument):
    result_percentile = 100 - instrument.result_percentile
    return result_percentile


def register_battery(battery_oid, assessment):
    url = f'http://{API_HOST}/ac_api/2014-01/BatteryAssessments/{battery_oid}.json'
    response = requests.post(url, auth=HTTPBasicAuth(REGISTRATION_OID, TOKEN))
    json = response.json()
    for instrument in json:
        instrument = Instrument(assessment=assessment,
                                expiration=pytz.UTC.localize(datetime.now()),
                                assessment_oid=instrument['OID'])
        instrument.save()


def create_assessment(patient_id, timezone, physician_id, screening, type, uuid=None):
    instruments = screening['instruments']

    # if timezone is passed in as string, convert it to pytz timezone object
    if isinstance(timezone, str):
        timezone = pytz.timezone(timezone)

    assessment = Assessment(patient_id=patient_id,
                            physician_id=physician_id,
                            progress=Assessment.NOT_STARTED,
                            date_created=datetime.now(timezone),
                            type=type,
                            uuid=uuid)
    assessment.save()

    if screening['title'] == 'PROMIS-29':
        register_battery(PROMIS29_OID, assessment)
        assessment.battery_assessment = True
        assessment.save()
        return assessment

    for instrument in instruments:
        # create custom instrument stuff for ORT. Otherwise, create normal Instrument
        if 'ort' in instrument and instrument['ort'] is True:
            ort_definition = CustomInstrumentDefinition.objects.get(name='Opioid Risk')
            instrument_object = CustomInstrumentInstance(assessment=assessment, definition=ort_definition)
            instrument_object.save()
            question_set = ort_definition.instrumentquestiondefinition_set.all()
            for question in question_set:
                question_instance = InstrumentQuestionInstance(definition=question,
                                                               instrument_instance=instrument_object)
                question_instance.save()
        elif instrument['oid']:
            instrument_object = Instrument(form_oid=instrument['oid'], assessment=assessment)
        else:
            continue
        instrument_object.save()
    return assessment


@login_required
def new_patient(request):
    provider_logo_url, user_name, timezone = get_logo_user_name_and_timezone(request)

    if request.method == 'POST':
        form = NewPatientForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']
            mrn = form.cleaned_data['mrn']
            month = form.cleaned_data['month']
            year = form.cleaned_data['year']
            day = form.cleaned_data['day']
            # first make phone a str so I can remove special characters and convert to int later
            phone = str(form.cleaned_data['phone'])
            email = form.cleaned_data['email']
            gender = form.cleaned_data['gender']

            # TODO: save email and phone number if present
            try:
                date_obj = datetime(year=int(year), month=int(month), day=int(day))
            except ValueError:
                messages.error(request, 'Invalid Date')
                return render(request, 'new_patient.html', {'form': form})

            post_data = {
                "name": name,
                "mrn": mrn,
                "dob": date_obj.strftime('%Y-%m-%d'),
                "gender": gender,
                "email": email,
                "mobile_number": phone
            }

            if phone:
                post_data['phone'] = phone
            if email:
                post_data['email'] = email

            user = request.user
            # Redirect to login page if user is not logged in
            if isinstance(user, AnonymousUser):
                url = reverse('django.contrib.auth.views.login')
                return HttpResponseRedirect(url)

            success, json_data = post_oauth('https://celerihealth.com/api/patients', post_data, request)
            if not success:
                if json_data is None:
                    return render(request, 'new_patient.html', {'form': form})
                else:
                    return json_data

            json_data = json_data.json()
            patient_id = json_data['data']['id']

            url = reverse('patient_detail', kwargs={'patient_id': patient_id})
            return HttpResponseRedirect(url)
        else:
            error_string = ''
            for key in form.errors:
                error_string += key + ': ' + form.errors[key] + ' '
            messages.error(request, strip_tags(error_string))

            payload = dict(
                (key, value)
                for key, value in form.data.items()
                if key not in ('csrfmiddlewaretoken',)
            )
            payload.update({'form': form})
            return render(request, 'new_patient.html', payload)
    else:
        form = NewPatientForm()

    return render(request, 'new_patient.html', {'form': form,
                                                'provider_logo_url': provider_logo_url,
                                                'user_name': user_name})


def battery_index(request):
    pass


def start_remote_assessment(request, assessment_uuid):

    if request.method == 'POST':
        assessment = Assessment.objects.get(uuid=assessment_uuid)
        register_instruments(assessment)
        return take_assessment(request, assessment.id)
    else:
        assessment = get_object_or_404(Assessment, uuid=assessment_uuid)
        return render(request, 'start_remote_assessment.html', {'assessment_uuid': assessment.uuid})


def start_assessment(request, assessment_id):
    if request.method == 'POST':
        assessment = Assessment.objects.get(id=assessment_id)
        register_instruments(assessment)

        # set the pin session False so patient cant get out of the assessment without providing passcode
        request.session[KEY_PIN_SESSION_VALID] = False
        request.session[KEY_CURRENT_ASSESSMENT_ID] = assessment.id
        return take_assessment(request, assessment_id)
    else:
        assessment = get_object_or_404(Assessment, pk=assessment_id)
        return render(request, 'start_assessment.html', {'assessment_id': assessment_id,
                                                         'patient_id': assessment.patient_id})


def register_instruments(assessment):
    """
    Before the patient takes the assessment, the individual instruments need to be registered against the PROMIS API

    :param assessment:
    :return:
    """
    url = REGISTER_INSTRUMENT_URL
    instruments = Instrument.objects.filter(assessment=assessment)
    # if this isn't a battery assessment, we need to register the individual instruments
    if not assessment.battery_assessment:
        for instrument in instruments:

            # Don't need to register custom instrument
            if isinstance(instrument, CustomInstrumentInstance):
                continue

            if instrument.assessment_oid is not None:

                if instrument.expiration is None:
                    print(f'WARNING: Instrument has assessment oid but no expiration date.')
                    instrument.expiration = datetime.strptime('1900-01-01 00:00:00+0000', '%Y-%m-%d %H:%M:%S%z')

                if not instrument.expiration < pytz.UTC.localize(datetime.now()):
                    print(
                        f'Instrument {instrument.form_oid} already has '
                        f'assessment OID and is not expired. Continuing...'
                    )
                    continue

            print(f'Registering {instrument.form_oid}')
            form_url = url.format(API_HOST=API_HOST, form_oid=instrument.form_oid)
            response = requests.post(form_url, auth=HTTPBasicAuth(REGISTRATION_OID, TOKEN))
            json = response.json()
            assessment_oid = json['OID']
            instrument.assessment_oid = assessment_oid

            instrument.expiration = datetime.strptime(json['Expiration'], '%m/%d/%Y %H:%M:%S %p')
            instrument.save()


def ort(request, instrument_id):
    provider_logo_url, user_name, timezone = get_logo_user_name_and_timezone(request)

    instrument = get_object_or_404(CustomInstrumentInstance, pk=instrument_id)
    if request.method == 'POST':
        form = OpioidRiskForm(request.POST)
        instrument.datetime_finished = datetime.now(pytz.timezone(timezone))
        instrument.save()

        # TODO: save results

        return take_assessment(request, instrument.assessment.id)
    else:
        form = OpioidRiskForm()
    return render(request, 'ort.html', {'form': form,
                                        'instrument': instrument})


def take_assessment(request, assessment_id):
    provider_logo_url, user_name, timezone = get_logo_user_name_and_timezone(request)

    assessment = get_object_or_404(Assessment, pk=assessment_id)
    first_question = False
    url = 'http://{API_HOST}/ac_api/2014-01/Participants/{assessment_oid}.json'

    # TODO: error check for if no instruments in set
    # TODO: handle case where all instruments are complete
    custom_instruments = CustomInstrumentInstance.objects.filter(assessment=assessment, datetime_finished__isnull=True)
    for instrument in custom_instruments:
        if request.method == 'POST':
            if assessment.progress == Assessment.NOT_STARTED:
                first_question = True
                assessment.progress = Assessment.IN_PROGRESS
                assessment.save()

            if not first_question and 'ItemResponseOID' in request.POST:
                answer_id = request.POST['ItemResponseOID']
                question_instance_id = request.POST['question_instance_id']
                question_instance = InstrumentQuestionInstance.objects.get(id=int(question_instance_id))
                question_instance.answer = InstrumentChoice.objects.get(id=answer_id)
                question_instance.save()

        unanswered_question = InstrumentQuestionInstance.objects.filter(
            answer__isnull=True,
            instrument_instance=instrument
        ).first()

        # no questions left, instrument is done
        if not unanswered_question:
            instrument.datetime_finished = datetime.now(pytz.timezone(timezone))
            instrument.save()
            # TODO: score results and save percentile, SE, etc.
            break

        question_text = unanswered_question.definition.text
        question_elements = [{
            'Description': question_text
        }]
        choices = unanswered_question.definition.instrumentchoice_set.all()
        answer_elements = []
        for choice in choices:
            answer_elements.append({
                'ItemResponseOID': choice.id,
                'Description': choice.get_text_display(),
                'question_instance_id': unanswered_question.id
            })

        return render(request, 'take_assessment.html', {'assessment_id': assessment_id,
                                                        'question_elements': question_elements,
                                                        'answer_elements': answer_elements})

    instruments = Instrument.objects.filter(assessment=assessment, datetime_finished__isnull=True)
    for instrument in instruments:

        if instrument.datetime_finished is not None:
            # instrument is already complete, continue to the next one
            continue

        request_data = {}
        if request.method == 'POST':
            if assessment.progress == Assessment.NOT_STARTED:
                first_question = True
                assessment.progress = Assessment.IN_PROGRESS
                assessment.save()

            body_unicode = request.body.decode('utf-8')
            answer_data = body_unicode.split('&')
            for param in answer_data:
                split = param.split('=')
                request_data[split[0]] = split[1]
        oid_url = url.format(API_HOST=API_HOST, assessment_oid=instrument.assessment_oid)
        response = requests.post(oid_url, auth=HTTPBasicAuth(REGISTRATION_OID, TOKEN), data=request_data)
        response_json = response.json()

        if 'Error' in response_json:
            logger.warning(f'Response error: {response_json["Error"]}')
            request.method = 'GET'
            continue

        # if date finished is present, the instrument is complete
        date_finished = response_json['DateFinished'].strip().replace("'", '')
        if date_finished:
            try:
                parsed_date_finished = parse(date_finished)
            except ValueError:
                print(f'WARNING: date_finished is invalid date: {date_finished}')
            else:
                instrument.datetime_finished = parsed_date_finished
                instrument.save()
                request.method = 'GET'
                continue

        # TODO: will there ever be more than one item?
        items = response_json['Items']
        for item in items:
            elements = item['Elements']
            question_elements = []
            answer_elements = []
            for element in elements:
                if 'Map' in element:
                    for choice in element['Map']:
                        answer_elements.append(choice)
                else:
                    question_elements.append(element)

            if first_question:
                return HttpResponseRedirect(reverse('take_assessment', kwargs={'assessment_id': assessment_id}))
            else:
                return render(request, 'take_assessment.html', {'assessment_id': assessment_id,
                                                                'question_elements': question_elements,
                                                                'answer_elements': answer_elements})

    request.session[KEY_CURRENT_ASSESSMENT_ID] = None

    assessment.progress = Assessment.COMPLETED
    assessment.save()

    request.session['next_url'] = reverse('thank_you', kwargs={'assessment_id': assessment_id})

    return HttpResponseRedirect(reverse('thank_you', kwargs={'assessment_id': assessment_id}))


def create_recommendation_and_educational_content(results):
    df = pd.DataFrame(results)
    df['cleaned_name'] = df['name'].str.extract(r"\s*-\s*(.*)", expand=False).str.replace(" 4a", "")

    # Invert percentile for physical function and ability to participate social
    # df['percentile'] = df.percentile.where(
    #     ~df.name.isin(["PROMIS Bank v2.0 - Social Participation", "PROMIS Bank v2.0 - Physical Function"]),
    #     100 - df.percentile.values)

    recommendation = df.cleaned_name.map(recommendation_db)
    recommendation = recommendation.where(df.sd > 1, None)
    recommendation = recommendation.dropna()
    recommendation = " ".join(recommendation.unique())
    # emotional support and physical function assessments scores have flipped interpretation
    df['sd'] = df.sd.where(~df.cleaned_name.isin(["Emotional Support", "Physical Function"]), -df.sd.values)

    if df.sd.max() < 1:
        return "None at this time, patient is within normal limits across all domains.", []

    pain_interference_sd = df[df['cleaned_name'] == 'Pain Interference']['sd'].iloc[0]
    anxiety_sd = df[df['cleaned_name'] == 'Anxiety']['sd'].iloc[0]
    depression_sd = df[df['cleaned_name'] == 'Depression']['sd'].iloc[0]
    fatigue_sd = df[df['cleaned_name'] == 'Fatigue']['sd'].iloc[0]
    phys_function_sd = df[df['cleaned_name'] == 'Physical Function']['sd'].iloc[0]
    sleep_disturbance_sd = df[df['cleaned_name'] == 'Sleep Disturbance']['sd'].iloc[0]
    # social_sd = df[df['cleaned_name'] == 'Social Participation']['sd'].iloc[0]

    recommendation = ''
    education = []
    if (
        pain_interference_sd > 0.5
        and anxiety_sd > 0.5
        and depression_sd > 0.5
        and fatigue_sd > 0.5
        and sleep_disturbance_sd > 0.5
    ):
        recommendation = (
            'Self-management strategies for mild mood symptoms.  Meditation '
            'techniques are tools that can be both therapeutic '
            'and performance-enhancing. '
        )
        education.append({
            'topic': 'Guided Body Scan Meditation',
            'url': 'https://youtu.be/i7xGF8F28zo'
        })
    if 0.5 < phys_function_sd < 2.0:
        recommendation += (
            'Self-management strategies for mild physical dysfunction include '
            'physician-directed home exercise programs. '
        )
        education.append({
            'topic': 'Yoga',
            'url': 'https://nccih.nih.gov/sites/nccam.nih.gov/files/yoga_infographic_NCCIH_update_FULL.jpg'
        })
    if 1.0 < phys_function_sd < 2.0:
        recommendation += (
            'Moderate physical dysfunction may be modifiable, if appropriate '
            'consider physical therapy and a personalized home exercise program. '
        )
        education.append({
            'topic': 'Exercise',
            'url': 'https://www.nia.nih.gov/health/exercise-physical-activity'
        })
    if 2.0 < phys_function_sd:
        recommendation += (
            'Severe physical dysfunction is present which may '
            'or may not be modifiable.  Consider fall prevention strategies. '
        )
        education.append({
            'topic': 'Fall Prevention',
            'url': 'https://www.nia.nih.gov/health/fall-proofing-your-home'
        })
    if pain_interference_sd > 1.0 and phys_function_sd > 1.0:
        recommendation += (
            'Self-management of moderate pain interference with daily activities includes pacing '
            'strategies. Chronic pain research has found that alternating between more and less '
            'physically demanding activities allows for gradual increase of activity levels and '
            'decrease in pain over time. '
        )
        education.append({
            'topic': 'Pacing',
            'url': ''
        })
    if anxiety_sd > 2.0 and depression_sd > 2.0:
        recommendation += (
            'Dual use medications for pain and mood may be appropriate to treat comorbid '
            'depression and/or anxiety. Consider non-pharmacologic behavioral health '
            'interventions. Consider referral to mental health provider. '
        )
        education.append({
            'topic': 'Mental Health Medications',
            'url': 'https://www.nimh.nih.gov/health/topics/mental-health-medications/index.shtml'
        })
    if fatigue_sd > 1.0 and sleep_disturbance_sd > 1.0:
        recommendation += ''
        education.append({
            'topic': 'Sleep',
            'url': 'https://www.ninds.nih.gov/Disorders/Patient-Caregiver-Education/Understanding-Sleep#7'
        })

    return recommendation, education


def get_educational_content():
    return 'test'


def post_results(results, recommendation, educational_content, assessment, assessment_history_data, oauth_token):
    oauth = OAuth2Session(CLIENT_ID, token=oauth_token)

    assessment_uuid = ''
    if assessment.uuid is not None:
        assessment_uuid = assessment.uuid

    post_data = {
        "patient_id": assessment.patient_id,
        "user_id": assessment.physician_id,
        "type": dict(Assessment.ASSESSMENT_TYPE_CHOICES).get(assessment.type).lower(),
        "assessment_uuid": assessment_uuid
    }

    report_data = {
        "assessment_date": assessment.date_created.strftime("%m/%d/%Y"),
        "recommendations": recommendation,
        "test_data": results,
        "assessment_history": assessment_history_data,
        "educational_content": educational_content
    }

    report_json = json.dumps(report_data)
    post_data['report'] = report_json

    try:
        oauth_response = oauth.post(url='https://celerihealth.com/api/reports', data=post_data)
    except Exception as e:
        logger.error('Unable to post assessment results', e)
        return False

    if oauth_response.status_code != 200:
        logger.error('Unable to post assessment results')
        return False

    response = json.loads(oauth_response.text)
    report_id = response['data']['id']

    return report_id


def finished_assessment(request, assessment_id):
    provider_logo_url, user_name, timezone = get_logo_user_name_and_timezone(request)

    assessment = get_object_or_404(Assessment, pk=assessment_id)
    patient, results = create_and_post_results(assessment, request)

    date_str = datetime.now(pytz.timezone(timezone)).strftime('%B %-d %Y')

    response = render(request, 'finished_assessment.html', {'assessment_id': assessment_id,
                                                            'results': results,
                                                            'patient_id': assessment.patient_id,
                                                            'provider_logo_url': provider_logo_url,
                                                            'user_name': user_name,
                                                            'patient': patient,
                                                            'date_str': date_str,
                                                            'report_id': assessment.report_id})

    return response


def create_and_post_results(assessment, request, oauth_token=None):
    patient = get_patient_info(request, assessment.patient_id)
    asmt_data, educational_content, recommendation, results = get_report_data(assessment, patient,
                                                                              request)
    # special case for remote assessments, where we need to pass in stored oauth token
    if not oauth_token:
        oauth_token = request.session['oauth_token']

    # if results have been reported yet, post them to backend API
    if not assessment.report_id:
        report_id = post_results(
            results,
            recommendation,
            educational_content,
            assessment,
            asmt_data,
            oauth_token
        )
        if report_id:
            assessment.report_id = report_id
            assessment.save()
    return patient, results


def get_report_data(assessment, patient, request):
    results = []
    ort_instrument = CustomInstrumentInstance.objects.filter(assessment=assessment).first()
    gender = patient['gender']
    # can't score ORT without gender
    if ort_instrument and gender:
        ort_score, positives = get_ort_score(gender, ort_instrument)
        results.append({
            'name': 'Opioid Risk Tool',
            'score': ort_score,
            'positives': positives
        })
    instruments = Instrument.objects.filter(assessment=assessment)
    for instrument in instruments:
        url = 'http://{api_host}/ac_api/2014-01/Results/{assessment_oid}.json'
        oid_url = url.format(assessment_oid=instrument.assessment_oid, api_host=API_HOST)
        response = requests.post(oid_url, auth=HTTPBasicAuth(REGISTRATION_OID, TOKEN))
        response_json = response.json()

        theta = response_json['Theta']
        std_error = response_json['StdError']

        # PROMIS-29 has a special instrument for pain intensity that needs to be handled differently
        if response_json['Name'] == 'PROMIS-Global Pain':
            if 'Items' not in response_json:
                continue

            items = response_json['Items']
            if len(items) > 1:
                continue

            # pain intensity only has one question, so we just need to get the response to the first item
            first_item = items[0]
            if 'Response' not in first_item:
                continue

            response = first_item['Response']
            try:
                response = int(response)
            except ValueError:
                logger.error(f'Unable to convert pain intensity to int: {response}')
                continue

            results.append({
                'name': response_json['Name'],
                'score': response
            })
            # continue to the next instrument
            continue

        if not theta:
            continue

        try:
            theta = float(theta)
        except ValueError:
            logger.error(f'Unable to convert theta to float: {theta}')
            t_score = None
            percentile = None
            standard_error = None
            sd = None
        else:
            t_score = (float(theta) * 10.0) + 50.0
            standard_error = float(std_error) * 10.0
            percentile = .5 * (math.erf(float(theta) / 2.0 ** .5) + 1.0)
            percentile = percentile * 100.0
            sd = (t_score - 50) / 10.0
            instrument.result_standard_error = standard_error
            instrument.result_t_score = t_score
            instrument.result_percentile = percentile

        instrument_name = response_json['Name']

        # Change name of ability to participate
        if instrument_name == 'PROMIS Bank v2.0 - Ability to Participate Social':
            instrument_name = 'PROMIS Bank v2.0 - Social Participation'

        results.append({
            'name': instrument_name,
            'standard_error': standard_error,
            't_score': t_score,
            'percentile': percentile,
            'sd': sd
        })

        instrument.name = instrument_name
        instrument.save()

    # TODO: why did I set next_url to finished assessment here? Remove?
    request.session['next_url'] = reverse('finished_assessment', kwargs={'assessment_id': assessment.id})

    assessments = Assessment.objects.filter(patient_id=assessment.patient_id)
    assessment_history = assessments.filter(progress=Assessment.COMPLETED).order_by('date_created')
    # get instrument names from this assessment to get historic values
    instrument_names = instruments.values_list('name', flat=True).distinct()
    asmt_data = get_longitudinal_chart_data(assessment_history, instrument_names, assessment)
    if assessment.type == Assessment.DIAGNOSTIC:
        recommendation, educational_content = create_recommendation_and_educational_content(results)
    else:
        recommendation = None
        educational_content = None
    return asmt_data, educational_content, recommendation, results


def get_ort_score(gender, ort_instrument):
    """
        id,text
        18,Do you have personal history of preadolescent sexual abuse?
        19,Do you have personal history of substance abuse with Rx drugs?
        20,Do you have personal history of substance abuse with illegal drugs?
        21,Do you have personal history of substance abuse with alcohol?
        22,Do you have family history of substance abuse with Rx drugs?
        23,Do you have family history of substance abuse with illegal drugs?
        24,Do you have family history of substance abuse with alcohol?
        25,"Do you have personal history of ADD, OCD, bipolar, or schizophrenia?"
        26,Do you have personal history of depression?
        """
    question_points_map = {
        18: {'male': 0, 'female': 3},
        19: {'male': 5, 'female': 5},
        20: {'male': 4, 'female': 4},
        21: {'male': 3, 'female': 3},
        22: {'male': 4, 'female': 4},
        23: {'male': 3, 'female': 2},
        24: {'male': 3, 'female': 1},
        25: {'male': 2, 'female': 2},
        26: {'male': 1, 'female': 1},
    }

    positives_map = {
        18: 'Personal history of preadolescent sexual abuse',
        19: 'Personal history of substance abuse with Rx drugs',
        20: 'Personal history of substance abuse with illegal drugs',
        21: 'Personal history of substance abuse with alcohol',
        22: 'Family history of substance abuse with Rx drugs',
        23: 'Family history of substance abuse with illegal drugs',
        24: 'Family history of substance abuse with alcohol',
        25: 'Personal history of ADD, OCD, bipolar, or schizophrenia',
        26: 'Personal history of depression'
    }
    ort_score = 0
    positives = []
    questions = InstrumentQuestionInstance.objects.filter(instrument_instance=ort_instrument)
    for question in questions:
        if question.answer.text == InstrumentChoice.YES:

            # lowercase all positives that come after the first one
            positives_text = positives_map[question.definition.id]
            if len(positives) > 0:
                positives_text = positives_text.lower()
            positives.append(positives_text)

            if gender == 'male':
                ort_score += question_points_map[question.definition.id]['male']
            elif gender == 'female':
                ort_score += question_points_map[question.definition.id]['female']
    if len(positives) == 0:
        return ort_score, None
    return ort_score, ', '.join(positives) + '.'


def thank_you(request, assessment_id):
    assessment = get_object_or_404(Assessment, pk=assessment_id)
    remote_assessment = False

    # if this is a remote assessment, report results to backend
    if assessment.uuid is not None:
        remote_assessment = True
        token_data_query = TokenData.objects.filter(user_id=assessment.physician_id).order_by('-expires')
        if len(token_data_query) > 1:
            token_data = token_data_query.first()
            create_and_post_results(assessment, request, token_data.token)
        else:
            logger.error('Unable to find token data to post remote assessment')

    return render(request, 'thank_you.html', {'assessment_id': assessment_id,
                                              'remote_assessment': remote_assessment})


class LoginView(LoginViewContrib):
    authentication_form = AuthenticationForm
