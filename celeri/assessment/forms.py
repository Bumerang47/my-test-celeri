from django import forms
from django.contrib.auth.forms import AuthenticationForm as ContribAuthenticationForm
from django.contrib.auth import authenticate
from django.utils.translation import ugettext_lazy as _
from phonenumber_field.formfields import PhoneNumberField


PROMIS29_OID = '784C754E-8707-4AF6-82DA-553BAFED59B2'

ASSESSMENT_CHOICES = (('D3FFE67C-FE0D-4DB4-B680-CDDBBA3B360B', 'PROMIS-Ca Bank v1.0 - Depression'),
                      ('F625E32A-B26D-4228-BBC2-DD6C2E4EEA6E', 'PROMIS Bank v1.0 - Sleep Disturbance'),
                      ('2AF69E2C-7571-475B-A5DE-E77AF1DF4A17', 'PROMIS SF v2.0 - Satisfaction Roles Activities 4a'),
                      ('8D79D960-EC12-4242-AE66-03EF8EF4D61D', 'PROMIS Bank v2.0 - Social Isolation'),
                      ('784C754E-8707-4AF6-82DA-553BAFED59B2', 'PROMIS-29 Profile/Battery v2.0'))

SCREEN_CHOICES = (('7224FB55-81D7-4131-A136-8C89907CDFBB', 'PROMIS Scale v1.0 - Pain Intensity 3a'),
                  ('6CAD2A91-2182-408F-BAF8-1F1F10F9D6BD', 'PROMIS-Ca Bank v1.1 - Pain Interference'))


month_choices = [
    ('1', '1 - January'),
    ('2', '2 - February'),
    ('3', '3 - March'),
    ('4', '4 - April'),
    ('5', '5 - May'),
    ('6', '6 - June'),
    ('7', '7 - July'),
    ('8', '8 - August'),
    ('9', '9 - September'),
    ('10', '10 - October'),
    ('11', '11 - November'),
    ('12', '12 - December'),
]

day_choices = []
for i in range(1, 32):
    day_choices.append((str(i), str(i)))

year_choices = []
for i in range(1900, 2018):
    year_choices.append((str(i), str(i)))

gender_choices = [
    ('M', 'Male'),
    ('F', 'Female')
]


class NewPatientForm(forms.Form):
    name = forms.CharField(max_length=100)
    mrn = forms.CharField(max_length=100)
    phone = PhoneNumberField(required=False)
    email = forms.EmailField(required=False)
    month = forms.ChoiceField(choices=month_choices)
    day = forms.ChoiceField(choices=day_choices)
    year = forms.ChoiceField(choices=year_choices)
    gender = forms.ChoiceField(choices=gender_choices)


class SelectScreeningsForm(forms.Form):
    instruments = forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple(attrs={'class': 'checkbox'}),
                                            choices=SCREEN_CHOICES)


class SelectAssessmentForm(forms.Form):
    instruments = forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple(attrs={'class': 'checkbox'}),
                                            choices=ASSESSMENT_CHOICES)


class CreatePinForm(forms.Form):
    pin = forms.CharField(label='pin', max_length=4)


class EnterPinForm(forms.Form):
    pin = forms.CharField(label='pin', max_length=4)


class OpioidRiskForm(forms.Form):
    YES = 'Y'
    NO = 'N'
    CHOICES = (
        (YES, 'YES'),
        (NO, 'NO')
    )
    family_history_alcohol = forms.CharField(max_length=10, widget=forms.Select(choices=CHOICES))
    family_history_illegal_drugs = forms.CharField(max_length=10, widget=forms.Select(choices=CHOICES))
    family_history_rx_drugs = forms.CharField(max_length=10, widget=forms.Select(choices=CHOICES))

    personal_history_alcohol = forms.CharField(max_length=10, widget=forms.Select(choices=CHOICES))
    personal_history_illegal_drugs = forms.CharField(max_length=10, widget=forms.Select(choices=CHOICES))
    personal_history_rx_drugs = forms.CharField(max_length=10, widget=forms.Select(choices=CHOICES))

    sexual_abuse = forms.CharField(max_length=10, widget=forms.Select(choices=CHOICES))

    psych_disease_1 = forms.CharField(max_length=10, widget=forms.Select(choices=CHOICES))
    psych_disease_2 = forms.CharField(max_length=10, widget=forms.Select(choices=CHOICES))


class AuthenticationForm(ContribAuthenticationForm):
    remember = forms.BooleanField(
        label=_("Remember me"),
        widget=forms.CheckboxInput,
        required=False,
    )

    def clean(self):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')
        remember = self.cleaned_data.get('remember')

        if username is not None and password:
            self.user_cache = authenticate(self.request, username=username, password=password, remember=remember)
            if self.user_cache is None:
                raise forms.ValidationError(
                    self.error_messages['invalid_login'],
                    code='invalid_login',
                    params={'username': self.username_field.verbose_name},
                )
            else:
                self.confirm_login_allowed(self.user_cache)

        return self.cleaned_data
