from django.contrib import admin
from .models import (
    Provider,
    Patient,
    Assessment,
    Instrument,
    CustomInstrumentDefinition,
    InstrumentQuestionDefinition,
    InstrumentChoice
)

admin.site.register(Provider)
admin.site.register(Patient)
admin.site.register(Assessment)
admin.site.register(Instrument)
admin.site.register(CustomInstrumentDefinition)
admin.site.register(InstrumentQuestionDefinition)
admin.site.register(InstrumentChoice)
