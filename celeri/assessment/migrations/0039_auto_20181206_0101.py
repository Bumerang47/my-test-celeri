# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2018-12-06 01:01
from __future__ import unicode_literals

import django.contrib.postgres.fields.jsonb
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('assessment', '0038_auto_20181206_0100'),
    ]

    operations = [
        migrations.AddField(
            model_name='tokendata',
            name='expires',
            field=models.BigIntegerField(null=True),
        ),
        migrations.AddField(
            model_name='tokendata',
            name='token',
            field=django.contrib.postgres.fields.jsonb.JSONField(null=True),
        ),
    ]
