# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2018-10-06 17:28
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('assessment', '0028_auto_20181006_1726'),
    ]

    operations = [
        migrations.CreateModel(
            name='InstrumentQuestionInstance',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('answer', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='assessment.InstrumentChoice')),
                ('definition', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='assessment.InstrumentQuestionDefinition')),
            ],
        ),
    ]
