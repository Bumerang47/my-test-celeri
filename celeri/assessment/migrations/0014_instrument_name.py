# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-07-21 23:44
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('assessment', '0013_auto_20180719_0004'),
    ]

    operations = [
        migrations.AddField(
            model_name='instrument',
            name='name',
            field=models.CharField(max_length=500, null=True),
        ),
    ]
