import json
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.core.urlresolvers import reverse
from requests_oauthlib import OAuth2Session
from requests.exceptions import RequestException
from .views import KEY_PIN_SESSION_VALID, CLIENT_ID


class PinExpiredMiddleware:

    def __init__(self, get_response):
        self.get_response = get_response
        # One-time configuration and initialization.

    def __call__(self, request):
        # Code to be executed for each request before
        # the view (and later middleware) are called.

        response = self.get_response(request)
        if not request.user.is_authenticated():
            return response

        pin_session_valid = request.session.get(KEY_PIN_SESSION_VALID, None)

        if pin_session_valid:
            return response

        if 'oauth_token' not in request.session:
            return response

        # If user does not have a pin set, redirect to create pin page
        oauth = OAuth2Session(CLIENT_ID, token=request.session['oauth_token'])
        try:
            oauth_response = oauth.get(url='https://celerihealth.com/api/pins')
        except RequestException:
            messages.error(request, 'Unable to get pin. Please contact support at support@celerihealth.com')
            return response

        if oauth_response.status_code != 200:
            messages.error(request, 'Unable to get pin. Please contact support at support@celerihealth.com')
            return response

        json_data = json.loads(oauth_response.text)
        has_pin = (len(json_data['data']) > 0)

        # TODO: replace creating_pin state with request URL check?
        creating_pin = request.path == reverse('create_pin')
        if not has_pin and not creating_pin:
            return HttpResponseRedirect(reverse('create_pin'))

        if (pin_session_valid is None or not pin_session_valid) and not creating_pin:
            # if already coming from enter_pin page or take_assessment just keep going
            if request.path == reverse('enter_pin') or \
                    'start_assessment' in request.path or \
                    'take_assessment' in request.path or \
                    'thank_you' in request.path or \
                    'ort' in request.path:
                return response
            return HttpResponseRedirect(reverse('enter_pin'))

        # TODO: support next_url stuff

        # check for invalid pin cookie. either expired, from the future, etc.

        return response
