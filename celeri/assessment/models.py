from django.contrib.auth.models import User
from django.contrib.postgres.fields import JSONField
from django.db import models
from phonenumber_field.modelfields import PhoneNumberField


class Provider(models.Model):
    user = models.OneToOneField(User, null=True, on_delete=models.CASCADE)
    backend_user_id = models.IntegerField(default=None, null=True)

    def __str__(self):
        return self.backend_user_id


class Patient(models.Model):
    name = models.CharField(max_length=300)
    patient_id = models.IntegerField(default=None, null=True)
    dob = models.DateField()
    mrn = models.CharField(max_length=200)
    email = models.EmailField(blank=True)
    phone = PhoneNumberField(blank=True)

    def __str__(self):
        return self.name


class Assessment(models.Model):
    NOT_STARTED = 'NS'
    IN_PROGRESS = 'IP'
    COMPLETED = 'C'
    ASSESSMENT_PROGRESS_CHOICES = (
        (NOT_STARTED, 'Not Started'),
        (IN_PROGRESS, 'In Progress'),
        (COMPLETED, 'Complete')
    )

    # These are only used when we post results for the pdf report
    SCREENING = 'S'
    DIAGNOSTIC = 'D'
    ASSESSMENT_TYPE_CHOICES = (
        (SCREENING, 'Screening'),
        (DIAGNOSTIC, 'Diagnostic'),
    )

    patient_id = models.IntegerField(default=None, null=True)
    physician_id = models.IntegerField(default=None, null=True)
    progress = models.CharField(max_length=10, choices=ASSESSMENT_PROGRESS_CHOICES, default=NOT_STARTED)
    date_created = models.DateTimeField(null=True)
    report_id = models.IntegerField(default=None, null=True)
    type = models.CharField(max_length=10, choices=ASSESSMENT_TYPE_CHOICES, default=DIAGNOSTIC)
    battery_assessment = models.BooleanField(default=False)
    uuid = models.CharField(max_length=100, null=True, db_index=True)

    def __str__(self):
        return str(self.id)


class Instrument(models.Model):
    name = models.CharField(max_length=500, null=True)
    form_oid = models.CharField(max_length=500, null=True)
    assessment = models.ForeignKey(Assessment, null=True, on_delete=models.CASCADE)
    assessment_oid = models.CharField(max_length=500, null=True)
    datetime_finished = models.DateTimeField(null=True)
    expiration = models.DateTimeField(null=True)
    result_standard_error = models.FloatField(null=True)
    result_t_score = models.FloatField(null=True)
    result_percentile = models.FloatField(null=True)

    def __str__(self):
        return self.form_oid


class CustomInstrumentDefinition(models.Model):

    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class CustomInstrumentInstance(models.Model):
    definition = models.ForeignKey(CustomInstrumentDefinition)
    assessment = models.ForeignKey(Assessment, null=True, on_delete=models.CASCADE, blank=True)
    datetime_finished = models.DateTimeField(null=True)
    result_standard_error = models.FloatField(null=True)
    result_t_score = models.FloatField(null=True)
    result_percentile = models.FloatField(null=True)


class InstrumentQuestionDefinition(models.Model):

    instrument = models.ForeignKey(CustomInstrumentDefinition)
    text = models.CharField(max_length=500)

    def __str__(self):
        return self.text


class InstrumentChoice(models.Model):

    YES = 'Y'
    NO = 'N'
    CHOICES = (
        (YES, 'Yes'),
        (NO, 'No'),
    )

    question = models.ForeignKey(InstrumentQuestionDefinition)
    text = models.CharField(max_length=10, choices=CHOICES, default=NO)

    def __str__(self):
        return f'{self.question.text}: {self.text}'


class InstrumentQuestionInstance(models.Model):

    definition = models.ForeignKey(InstrumentQuestionDefinition)
    answer = models.ForeignKey(InstrumentChoice, null=True)
    instrument_instance = models.ForeignKey(CustomInstrumentInstance, null=True)

    def __str__(self):
        return self.definition.text


class TokenData(models.Model):

    user_id = models.IntegerField()
    token = JSONField(null=True)
    expires = models.BigIntegerField(null=True)