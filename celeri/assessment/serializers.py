from datetime import datetime
from django.utils import timezone
from rest_framework import serializers
from .models import Assessment, Instrument


class AssessmentSerializer(serializers.Serializer):
    patient_id = serializers.IntegerField(read_only=True)
    instruments = serializers.ListField(
        child=serializers.CharField(max_length=100)
    )

    def create(self, validated_data):
        """
        Create and return a new `Snippet` instance, given the validated data.
        """
        patient_id = validated_data.get('patient_id')
        instruments = validated_data.get('instruments')

        assessment = Assessment(patient_id=patient_id,
                                progress=Assessment.NOT_STARTED,
                                date_created=datetime.now())

        for instrument_oid in instruments:
            instrument_object = Instrument(form_oid=instrument_oid, assessment=assessment)
            instrument_object.save()

        return Assessment.objects.create(**validated_data)


class DateTimeFieldWihTZ(serializers.DateTimeField):
    """Class to make output of a DateTime Field timezone aware
    """

    def to_representation(self, value):
        # value = timezone.localtime(value)
        value = value.astimezone(timezone.get_current_timezone())
        # return super().to_representation(value)
        return super().to_representation(value)


class NewestAssessmentSerializer(serializers.ModelSerializer):
    report_id = serializers.ReadOnlyField(read_only=True)
    date_created = DateTimeFieldWihTZ(format='%b. %d, %Y, %H:%M %P', read_only=True)

    class Meta:
        model = Assessment
        fields = ('report_id', 'date_created')


class PatientSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    name = serializers.CharField(max_length=200)
    mrn = serializers.CharField(max_length=200)
    dob = serializers.DateTimeField()
    newest_assessment = serializers.SerializerMethodField('get_assessment')

    class Meta:
        fields = ('id', 'name', 'mrn', 'dob', 'assessment')

    @staticmethod
    def get_assessment(obj):
        assessment_set = Assessment.objects.filter(
            patient_id=obj.get('id'),
            report_id__isnull=False
        ).order_by('-date_created')
        assessment = NewestAssessmentSerializer(assessment_set.first())
        return assessment.data


class PaginateSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    name = serializers.CharField(max_length=200)
    mrn = serializers.CharField(max_length=200)
    dob = serializers.DateTimeField()
    newest_assessment = serializers.SerializerMethodField('get_assessment')

    class Meta:
        fields = ('id', 'name', 'mrn', 'dob', 'assessment')

    @staticmethod
    def get_assessment(obj):
        assessment_set = Assessment.objects.filter(
            patient_id=obj.get('id'),
            report_id__isnull=False
        ).order_by('-date_created')
        assessment = NewestAssessmentSerializer(assessment_set.first())
        return assessment.data
