import json

from django.contrib.auth import SESSION_KEY, HASH_SESSION_KEY, _get_user_session_key
from django.contrib.auth.models import User
from django.utils.crypto import constant_time_compare
from requests_oauthlib import OAuth2Session
from oauthlib.oauth2 import LegacyApplicationClient
from oauthlib.oauth2.rfc6749.errors import MissingTokenError

from .views import CLIENT_ID, CLIENT_SECRET
from .models import Provider, TokenData

token_url = 'https://celerihealth.com/oauth/token'


class OAuthBackend:
    """
    Authenticate against the settings ADMIN_LOGIN and ADMIN_PASSWORD.

    Use the login name and a hash of the password. For example:

    ADMIN_LOGIN = 'admin'
    ADMIN_PASSWORD = 'pbkdf2_sha256$30000$Vo0VlMnkR4Bk$qEvtdyZRWTcOsCnI/oQ7fVOu1XAURIZYoOZ3iq8Dr4M='
    """

    def authenticate(self, request, username=None, password=None, remember=None):

        oauth = OAuth2Session(client=LegacyApplicationClient(client_id=CLIENT_ID))
        try:
            token = oauth.fetch_token(token_url=token_url,
                                      username=username, password=password, client_id=CLIENT_ID,
                                      client_secret=CLIENT_SECRET)
        except MissingTokenError:
            # invalid credentials
            return None

        user_info = oauth.get('https://celerihealth.com/api/info')
        json_data = json.loads(user_info.text)

        user_data = json_data['data']['user'][0]
        email = user_data['email']
        user_id = user_data['id']

        # save user_id and oauth token in DB so I can use it to post remote_assessment results
        try:
            token_data = TokenData.objects.get(user_id=user_id, token=token)
        except TokenData.DoesNotExist as dne:
            token_data = TokenData(user_id=user_id, token=token, expires=int(token['expires_at']))
            token_data.save()

        # TODO: If there is already a user in this db with all matching info, use that one. If not, create new
        # TODO: create user with username = ID from backend (because username is unique constraint for Django User)
        try:
            user = User.objects.get(username=username)
        except User.DoesNotExist:
            user = User(username=username, email=email)
            user.save()
            provider = Provider(user=user, backend_user_id=user_id)
            provider.save()

        # automatically create provider linked to user if it doesn't already exist
        try:
            Provider.objects.get(user=user)
        except Provider.DoesNotExist:
            provider = Provider(user=user, backend_user_id=user_id)
            provider.save()

        # The django login flushes the session if the user logging in doesn't match the session key, but we need to
        # set the oauth token on the session. To get around this, I'm "short circuiting" the session flush that would
        # happen after setting the oauth_token below
        if hasattr(user, 'get_session_auth_hash'):
            session_auth_hash = user.get_session_auth_hash()

        if SESSION_KEY in request.session:
            if _get_user_session_key(request) != user.pk or (
                    session_auth_hash and
                    not constant_time_compare(request.session.get(HASH_SESSION_KEY, ''), session_auth_hash)):
                request.session.flush()

        # TODO: use token expiration to set session expire on user

        # save the oauth client to the session
        request.session['oauth_token'] = token

        if remember:
            request.session.set_expiry(0)

        return user

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None
