
from django import template

register = template.Library()


@register.filter
def pagination_range(obj, current=1, limit=10):
    """ Used with pagination page_range object when you have a lot of pages
    Use within a template with a paginator:
    {% for page in obj_list.paginator.page_range|pagination_limit:obj_list.number %}
        {{ page }}
    {% endfor %}

    """

    left = (limit / 2) + 1
    right = limit / 2
    total = len(obj)

    if limit % 2 == 0:
        right -= 1

    if current < left:
        return obj[:limit]
    if current > total - right:
        return obj[total-limit:]

    return obj[current-left:current+right]
