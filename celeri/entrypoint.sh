#!/bin/bash

# Prepare log files and start outputting logs to stdout
mkdir -p ./logs
touch ./logs/gunicorn.log
touch ./logs/gunicorn-access.log
tail -n 0 -f ./logs/gunicorn*.log &

export DJANGO_SETTINGS_MODULE=celeri_root.settings
export SSL_PATH=$(echo $(python -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")\
/sslserver/certs/)

exec gunicorn celeri_root.wsgi:application \
    --bind unix:/tmp/gunicorn_run/gunicorn.sock \
    --timeout 30 \
    --workers 5 \
    --name celeri_django \
    --log-level=info \
    --log-file=./logs/gunicorn.log \
    --access-logfile=./logs/gunicorn-access.log \
"$@"