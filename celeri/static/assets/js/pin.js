const empty_html_text = "<li></li><li></li><li></li><li></li>";
const one_digit_text = "<li class='filled'></li><li></li><li></li><li></li>";
const two_digit_text = "<li class='filled'></li><li class='filled'></li><li></li><li></li>";
const three_digit_text = "<li class='filled'></li><li class='filled'></li><li class='filled'></li><li></li>";
const four_digit_text = "<li class='filled'></li><li class='filled'></li><li class='filled'></li><li class='filled'></li>";
const close_button = $('<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>')

var current_pin = '';
var the_pin = '';
var second_attempt = false;
var first_pin = '';
var start_text = '';
var confirm_text = 'Confirm passcode';
var confirm_erorr = 'Passcodes do not match';

var set_start_text = function(text) {
    start_text = text;
}

var reset_pin = function () {
    current_pin = '';
    second_attempt = false;
    update_pin_display();
};
var append_to_pin = function (input) {
    current_pin += input;
    update_pin_display();
    // attempt_auth();
};

var handle_create_pin_complete = function () {
    let html_text = null;
    if (second_attempt === false) {
        $(".notifications").empty()
        first_pin = the_pin;
        $('#pin_title').html(confirm_text);
        html_text = empty_html_text;

        current_pin = '';
        the_pin = '';
        second_attempt = true;
    } else {
        html_text = four_digit_text;
        $('#pin_title').html(start_text);
        if (first_pin !== current_pin) {
            $(".notifications").append(
                $('<div class="alert alert-danger alert-dismissible fade show"></div>')
                  .append(close_button).append(confirm_erorr)
            )

            second_attempt = false;
            current_pin = '';
            the_pin = '';
            first_pin = '';
            html_text = empty_html_text;
        } else {
            document.getElementById("pin").value = the_pin;
            document.getElementById("pin-form").submit();
        }
    }
    return html_text
};

var handle_enter_pin_complete = function () {
    document.getElementById("pin").value = the_pin;
    document.getElementById("pin-form").submit();
};

var update_pin_display = function () {
    let html_text = null;

    if (current_pin === '') {
        html_text = empty_html_text;
        $('#confirm_pin').html(start_text);
    } else {
        the_pin = current_pin;
        if (the_pin.length === 1) {
            html_text = one_digit_text;
        } else if (the_pin.length === 2) {
            html_text = two_digit_text;
        } else if (the_pin.length === 3) {
            html_text = three_digit_text;
        } else { // four digits
            if (creating_pin_mode === true) {
                html_text = handle_create_pin_complete();
            } else {
                html_text = handle_enter_pin_complete();
            }
        }
    }

    $('#current_pin').html(html_text);
};

$(document).ready(function () {
    set_start_text($('#pin_title').text())
    $(document).keypress(function (e) {
        var key_number = String.fromCharCode(e.which)
        append_to_pin(key_number);
    });
    $('.pin_input').click(function () {
        append_to_pin(this.innerHTML);
    });
    $('.reset').click(reset_pin);
});