"""
WSGI config for celeri project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.11/howto/deployment/wsgi/
"""

# Unlike standard Python functionality, the import order matters: the New Relic package must be imported first.
import os
import newrelic.agent
newrelic.agent.initialize(os.path.join(os.path.dirname(__file__), "newrelic.ini"))


from django.core.wsgi import get_wsgi_application  # noqa: E402

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "celeri.settings")

application = get_wsgi_application()
