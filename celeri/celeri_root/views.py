from django.shortcuts import render_to_response
from assessment.views import get_logo_user_name_and_timezone, SUPPORT_MSG


def base_handler(request):
    header_info = {}
    if request.user.is_authenticated:
        provider_logo_url, user_name, timezone = get_logo_user_name_and_timezone(request)
        header_info.update({
            'user_name': user_name,
            'provider_logo_url': provider_logo_url,
        })
    return header_info


def e_handler404(request):
    header_info = base_handler(request)
    return render_to_response('404.html', header_info, status=404,)


def e_handler500(request):
    base_info = base_handler(request)
    base_info['message'] = SUPPORT_MSG
    return render_to_response('500.html', base_info, status=500,)
