    function draw_longitudinal_chart(div_id, series) {
        Highcharts.chart(div_id, {

                title: {
                    text: 'Assessment History'
                },

                chart: {
                    type: 'line',
                    backgroundColor: '#f9f9f9',
                    height: 365,
                },

                yAxis: {
                    title: {
                        text: 'Percentile'
                    }
                },
                xAxis: {
                    type: 'datetime',
                    labels: {
                        format: '{value:%m/%d/%y}',
                        enabled: false
                    }
                },
                legend: {
                    itemStyle: {
                        color: '#1a1a1a',
                        fontWeight: 'bold',
                        fontSize:'12px'
                    }
                },
                tooltip: {
                    headerFormat: '<b>{series.name}</b><br>',
                    pointFormat: '{point.x:%m/%d/%y}: {point.y:.2f}'
                },
                plotOptions: {
                    series: {
                        label: {
                            connectorAllowed: false
                        },
                    },
                    spline: {
                        marker: {
                            enabled: true
                        }
                    }
                },

                series: series,

                responsive: {
                    rules: [{
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom'
                            }
                        }
                    }]
                },
                credits: {
                      enabled: false
                }

            });
    }