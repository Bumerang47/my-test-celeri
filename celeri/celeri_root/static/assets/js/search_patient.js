(function () {
    "use strict";
    var statusRequestTimeout, pagination_display, patient_row;

    function pagination_distance(current, count, limit) {
        limit = limit || 10;

        if (count < limit) {
            limit = count;
        }

        var left = (limit / 2) + 1;
        var right = limit / 2;

        if (limit % 2 === 0) {
            right -= 1;
        }
        var leftEdge = Math.max(1, current - left);
        if (current < left) {
            return {left: 1, right: limit};
        }
        if (current > count - right) {
            return {left: leftEdge, right: count};
        }
        return {left: leftEdge, right: current + right};
    }
    function displaying_patients(data) {
        var table_body = $("#patient-table").find("tbody");
        table_body.empty();
        pagination_display(data);
        if (!data.data.length) {
            data.data = [{
                id: "",
                name: "&nbsp;",
                mrn: "",
                dob: "",
                newest_assessment: {report_id: "", date_created: ""}
            }];
        }
        data.data.forEach(function (patient) {
            var new_row = $(patient_row(patient));
            table_body.append(new_row);
        });
        $(".clickable-element").click(function () {
            window.location = $(this).data("href");
        });
    }
    function load_patients(text, index_page, page_size) {
        index_page = index_page || 1;
        page_size = page_size || 1;
        var url = new URL("/api/patients/", window.location),
            el_status = $(".search-con .status_searching");
        var params = {
            filter: text,
            page: index_page,
            page_size: page_size || ""
        };
        Object.keys(params).forEach(function (key) {
            url.searchParams.append(key, params[key]);
        });
        el_status.text("Request in progress ...");
        clearTimeout(statusRequestTimeout);

        axios.get(url).then(function (res) {
            el_status.text("Request finished.");
            statusRequestTimeout = setTimeout(function () {
                el_status.empty();
            }, 3000);
            displaying_patients(res.data);
        }).catch(function (e) {
            console.info("something went wrong: " + e);
            el_status.text("Request failed.");
        });
    }

    function load_pagination_page(ev) {
        var new_page = $(ev.currentTarget).attr("data-page");
        var filter = $("#patient-search").val();
        if ($(ev.currentTarget).hasClass("disabled")) {
            return;
        }
        $(ev.currentTarget).parent().children().removeClass("active");
        $(ev.currentTarget).addClass("active");
        load_patients(filter, new_page);
    }

    pagination_display = function (data) {
        var prevPageLi, nextPageLi,
                pagination_pane = $("#patient-table").next(".pagination"),
                currentPage = data.current_page,
                lastPage = data.last_page;

        pagination_pane.empty();
        prevPageLi = $(
            "<li class=\"page-item\""
            + (
                currentPage > 1
                ? ""
                : "disabled"
            ) + "><a class=\"page-link\" "
            + "href=\"javascript:void(0)\">Previous</a></li>"
        );
        $(prevPageLi).attr("data-page", Math.max(1, currentPage - 1));
        pagination_pane.append(prevPageLi);

        var distance = pagination_distance(currentPage, lastPage);

        var i;
        var item;
        for (i = distance.left; i <= distance.right; i += 1) {
            item = "";
            if (currentPage === i) {
                item = $(
                    "<li class=\"page-item active\">"
                    + "<a class=\"page-link\" href=\"javascript:void(0)\">" + i
                    + "<span class=\"sr-only\">(current)</span></a></li>"
                );
            } else {
                item = $(
                    "<li class=\"page-item\"><a class=\"page-link\" "
                    + "href=\"javascript:void(0)\">" + i + "</a></li>"
                );
            }
            item.attr("data-page", i);
            pagination_pane.append(item);
        }
        nextPageLi = $(
            "<li class=\"page-item "
            + (
                currentPage < lastPage
                ? ""
                : "disabled"
            )
            + "\"><a class=\"page-link\" href=\"javascript:void(0)\">Next</a></li>"
        );
        $(nextPageLi).attr("data-page", String(currentPage + 1));
        pagination_pane.append(nextPageLi);
        pagination_pane.children().on("click", load_pagination_page);
    };

    function searchTable() {
        // Declare variables
        var input;
        var filter;
        input = document.getElementById("patient-search");
        filter = input.value.toUpperCase();

        if (filter.length > 2 || !filter) {
            load_patients(filter);
        }
    }

    patient_row = function (patient) {
        var drop_down = "<div class=\"dropdown-con\"><div class=\"icon-con\">"
                + "<a href=\"https://celerihealth.com/reports/view/" + patient.newest_assessment.report_id + "\"><i class=\"fa fa-file-text-o\"></i></a></div>"
                + "<div class=\"dropdown\"><a class=\"dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"fa fa-caret-down\"></i></a>"
                + "<div class=\"dropdown-menu\">"
                + "<a class=\"dropdown-item\" href=\"https://celerihealth.com/reports/view/" + patient.newest_assessment.report_id + "\"><i class=\"fa fa-eye\"></i> View</a>"
                + "<a class=\"dropdown-item\" href=\"https://celerihealth.com/reports/download/" + patient.newest_assessment.report_id + "\" media=\"print\"><i class=\"fa fa-print\"></i> Print</a>"
                + "</div>"
                + "</div>"
                + "</div>";

        var template_row = (
            "<tr>"
            + "<td class=\"clickable-element\" data-href=\"/assessment/patient_detail/" + (patient.id || "") + "\">" + patient.name + " </td> "
            + "<td class=\"clickable-element\" data-href=\"/assessment/patient_detail/" + (patient.id || "") + "\">" + patient.mrn + " </td> "
            + "<td class=\"clickable-element\" data-href=\"/assessment/patient_detail/" + (patient.id || "") + "\">" + patient.dob + " </td> "
            + "<td>" + (
                patient.newest_assessment.report_id
                ? drop_down
                : ""
            ) + " </td> "
            + " <td> " + (patient.newest_assessment.date_created || "") + " </td> "
            + " </tr> "
        );
        return template_row;
    };

    $(document).ready(function () {
        var delay = 500; // half second
        $(".clickable-element").click(function () {
            window.location = $(this).data("href");
        });
        $("#patient-search").on("input", _.debounce(searchTable, delay));
        $(".pagination > .page-item").on("click", load_pagination_page);

    });
}());
