"""celeri URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from assessment.views import LoginView
from .views import e_handler404, e_handler500

handler404 = e_handler404
handler500 = e_handler500

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^', include('assessment.urls')),  # NOTE: without $
    url(r'^assessment/', include('assessment.urls')),
    url(r'^api/', include('assessment.api_urls')),
    url('^accounts/login', LoginView.as_view()),
    url('^accounts/', include('django.contrib.auth.urls')),
    url(r'^api-auth/', include('rest_framework.urls'))
]
